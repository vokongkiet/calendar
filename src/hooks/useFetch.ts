import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';

import { Notification } from 'farmhub-ui-core';

const paginationInit = {
  current: 1,
  next: 2,
  prev: 0,
  take: 10,
  total: 0,
};

interface Props {
  form?: any;
  initField?: string;
  selector: any;
  action: any;
  query?: any;
  initValue?: Array<any>;
  deps?: Array<any>;
  isVisible?: boolean;
}

const useFetch = ({
  form,
  initField,
  selector,
  action,
  query = {},
  initValue = [],
  deps = [],
  isVisible = true,
}: Props) => {
  const dispatch = useDispatch();

  const data: Array<any> = useSelector(selector);
  const [pagination, setPagination] = useState(paginationInit);
  const [error, setError] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!isVisible) {
      return;
    }

    let isMounted = true;
    const fetchData = async () => {
      setIsLoading(true);
      try {
        if (!query) {
          return;
        }
        const actionResult = await dispatch(action(query));
        const { pagination: paginationResult, data } =
          unwrapResult(actionResult);
        if (form && initField && data.length > 0) {
          form.setFieldsValue({ [initField]: data[0].id });
        }

        if (isMounted) {
          setPagination(paginationResult);
        }
      } catch (err: any) {
        if (isMounted) {
          setError(err);
          Notification('error', 'Tải dữ liệu thất bại', err);
        }
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();

    return () => {
      isMounted = false;
    };
  }, deps);//eslint-disable-line

  return {
    isLoading,
    error,
    data: initValue ? [...initValue, ...data] : data,
    pagination,
  };
};

export default useFetch;
