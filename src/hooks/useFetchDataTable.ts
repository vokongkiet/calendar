import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { unflatten } from 'flat';

import { Notification } from 'farmhub-ui-core';
// TODO: circle import
import useQueryParam from 'hooks/useQueryParam';

const paginationInit = {
  current: 1,
  next: 2,
  prev: 0,
  take: 10,
  total: 0,
};

interface Props {
  isVisible?: boolean;
  selector: any;
  action: any;
  query: any;
  option: any;
  deps?: any;
}

const useFetchDataTable = ({
  isVisible = true,
  selector,
  action,
  query = {},
  option = {},
  deps = [],
}: Props) => {
  const dispatch = useDispatch();

  const data: any = useSelector(selector);
  const [pagination, setPagination] = useState(paginationInit);
  const [isLoading, setIsLoading] = useState(false);

  useQueryParam({ query, option, deps });

  useEffect(() => {
    let isMounted = true;

    const orderQuery: any = {};
    orderQuery[query.order?.name] = query.order?.value;

    const standardQuery = unflatten({ ...query });

    const fetchData = async () => {
      setIsLoading(true);
      try {
        if (!query) {
          return;
        }

        const actionResult = await dispatch(
          action({
            ...standardQuery,
            order: orderQuery,
          }),
        );
        const { pagination: paginationResult } = unwrapResult(actionResult);

        if (isMounted) {
          setPagination(paginationResult);
        }
      } catch (err: any) {
        Notification('error', 'Tải dữ liệu thất bại', err);
      } finally {
        setIsLoading(false);
      }
    };

    if (isVisible) {
      fetchData();
    }

    return () => {
      isMounted = false;
    };
  }, [...deps, isVisible]); //eslint-disable-line

  return { isLoading, data, pagination, query };
};

export default useFetchDataTable;
