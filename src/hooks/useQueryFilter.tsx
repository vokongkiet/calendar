import { useEffect } from 'react';
import queryString from 'query-string';
import { parseQueryString } from 'utils/function';
import { useLocation } from 'react-router-dom';

interface Props {
  form: any;
  query: any;
  setQuery: (e: any) => void;
}

const useQueryFilter = ({ form, query, setQuery }: Props) => {
  const location = useLocation();

  useEffect(() => {
    const initFilter = () => {
      let fieldKeys = queryString.parse(location.search);
      if (Object.keys(fieldKeys).length === 0) {
        return;
      }

      parseQueryString(fieldKeys);

      Object.keys(fieldKeys).map(key => {
        if (key !== 'order') {
          if (
            !String(query[key]) ||
            String(query[key]) !== String(fieldKeys[key])
          ) {
            setQuery({ ...query, ...fieldKeys });
            form.setFieldsValue({
              ...fieldKeys,
            });
          }
        } else {
          if (
            fieldKeys.order.name !== query.order.name ||
            fieldKeys.order.value !== query.order.value
          ) {
            setQuery({ ...query, ...fieldKeys });
            form.setFieldsValue({
              ...fieldKeys,
            });
          }
        }
      });
    };

    initFilter();
  }, [location, form]);
};

export default useQueryFilter;
