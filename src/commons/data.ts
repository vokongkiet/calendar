export const entityTypes: EntityTypesProps = {
  0: 'user',
  1: 'farm',
  2: 'store',
  3: 'collaborator',
  4: 'warehouse',
  5: 'transport',
  6: 'volunteer',
};

export const personStatusList = [
  { key: 'all', value: 'all', name: 'Tất cả các trạng thái' },
  { key: '0', value: 0, name: 'Đang hoạt động' },
  { key: '1', value: 1, name: 'Dừng hoạt động' },
  { key: '2', value: 2, name: 'Tạm dừng hoạt động' },
];

export const skeletonList = new Array(10).fill('').map((e, idx) => ({
  key: idx,
}));

interface EntityTypesProps {
  [key: number]: string;
}
