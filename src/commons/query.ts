

export const userOption = {
  select: [
    'id',
    'displayName',
    'email',
    'avatar',
    'avatarThumbnail',
    'type',
    'phoneNumber',
    'website',
    'totalFarmAccount',
    'totalCoBuy',
    'totalStoreAccount',
    'totalCollaboratorAccount',
    'totalWarehouseAccount',
    'totalTransportAccount',
    'totalVolunteerAccount',
  ],
};
