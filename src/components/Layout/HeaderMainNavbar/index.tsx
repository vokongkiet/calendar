import { Layout } from 'antd';
import { RootState } from 'app';
import { Action, IconWrapper, NameText, TinyText } from 'components';
import { updateMenuToggle } from 'containers/App/Account/entitySlice';
import {
  CaretDownOutlined,
  Dropdown,
  Icon,
  LeftOutlined,
  LogoutOutlined,
  RightOutlined,
  Spacing,
  UserOutlined,
} from 'farmhub-ui-core';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import farmer from 'assets/images/farmer.png';

import { truncate } from 'utils/function';

const { Header } = Layout;

interface HeaderMainNavbarProps {
  onLogoutUser: (e: any) => void;
}

const HeaderMainNavbar = ({
  onLogoutUser,
}: HeaderMainNavbarProps): JSX.Element => {
  const dispatch = useDispatch();
  const isToggle = useSelector((state: RootState) => state.entity.isToggle);

  const dropdownItems = [
    <UserInfo>
      <Spacing direction="vertical">
        <NameText lineNumber={2}>Nguyen Van A</NameText>
        <TinyText>vana@gmail.com</TinyText>
      </Spacing>
    </UserInfo>,
    <Action
      onClick={() => {}}
      dropdownItemProps={{
        icon: {
          children: (
            <Icon>
              <UserOutlined />
            </Icon>
          ),
        },
        key: '1',
      }}
    >
      Thông tin của tôi
    </Action>,
    <Action
      onClick={onLogoutUser}
      dropdownItemProps={{
        icon: {
          children: (
            <Icon>
              <LogoutOutlined />
            </Icon>
          ),
        },
        key: '2',
      }}
    >
      Thoát tài khoản
    </Action>,
  ];

  return (
    <Wrapper id="header_main">
      <DivRow>
        <IconWrapper
          displayBackground={true}
          background="#F8F8F8"
          onClick={() => dispatch(updateMenuToggle(!isToggle))}
          size={36}
          icon={
            <Icon>
              {isToggle ? (
                <LeftOutlined style={{ fontSize: 14, color: '#595959' }} />
              ) : (
                <RightOutlined style={{ fontSize: 14, color: '#595959' }} />
              )}
            </Icon>
          }
        />
      </DivRow>
      <DivRow>
        <Dropdown arrow items={dropdownItems} containerId="header_main">
          <WrapperAvatar>
            <Avatar src={farmer} />
            <InfoAvatar>
              <TinyText>Tài khoản</TinyText>
              <DivRow>
                <span className="name-text">
                  {truncate('Nguyen Van A', 16)}
                </span>
                <CaretDownOutlined />
              </DivRow>
            </InfoAvatar>
          </WrapperAvatar>
        </Dropdown>
      </DivRow>
    </Wrapper>
  );
};

export default HeaderMainNavbar;

const Wrapper = styled(Header)`
  display: flex;
  height: 68px;
  justify-content: space-between;
  padding: 2px 18px;
  align-items: center;
  background: #ffffff;
`;

const Avatar = styled.img`
  width: 42px;
  height: 42px;
  border: 2px solid #ffffff;
  box-sizing: border-box;
  filter: drop-shadow(0px 4px 6px rgba(0, 0, 0, 0.1));
  border-radius: 50%;
  margin-right: 12px;
`;

const DivRow = styled.div`
  display: flex;
  width: auto;
  align-items: center;
`;

const UserInfo = styled.div`
  width: 300px;
  padding: 24px;
  font-weight: ${props => props.theme.font.weight.regular};
  border-bottom: 1px solid #f1f1f1;
`;

const WrapperAvatar = styled.div`
  width: auto;
  height: 42px;
  display: flex;
  align-items: center;
  .name-text {
    font-weight: ${props => props.theme.font.weight.bold};
    font-size: ${props => props.theme.font.size.xs};
    line-height: 22px !important;
    margin-right: 12px;
  }
  cursor: pointer;
`;

const InfoAvatar = styled.div`
  display: flex;
  width: 160px;
  height: 42px;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
`;
