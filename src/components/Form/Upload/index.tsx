import styled from 'styled-components';
import { Upload as AntUpload } from 'antd';
import { FileImageOutlined } from 'farmhub-ui-core';

const CustomUpload = styled(AntUpload)<{
  width: string;
  height: string;
  isUrl: boolean;
}>`
  position: relative;
  padding: 12px;
  border: 1px solid #f1f1f1;
  border-radius: 6px;
  width: ${props => props.width};
  display: flex;
  justify-content: center;

  .avatar-uploader > .ant-upload {
    width: 100%;
    height: ${props => props.height};
    background: #f6fcf9;
  }

  .ant-upload.ant-upload-select-picture-card {
    width: ${props => (props.isUrl ? 'auto' : '100%')};
    height: ${props => props.height};
    border: none;
    background: #f6fcf9;
    margin-right: 0px;
    margin-bottom: 0px;
  }

  .ant-upload.ant-upload-select-picture-card img {
    width: 100%;
    height: ${props => props.height};
    background: #f6fcf9;
  }
`;

const Img = styled.img<{
  width: string;
  height: string;
}>`
  width: 100% !important;
  height: ${props => props.height} !important;

  object-fit: cover;
`;

interface UploadProps {
  width: string;
  height: string;
  disabled?: boolean;
  value?: any;
  onChange?: any;
}

const Upload = ({
  disabled,
  width,
  height,
  value,
  onChange,
}: UploadProps): JSX.Element => {
  return (
    <CustomUpload
      width={width}
      height={height}
      isUrl={value === '' ? false : true}
      disabled={disabled}
      onChange={onChange}
      name="avatar"
      listType="picture-card"
      className="avatar-uploader"
      showUploadList={false}
      beforeUpload={file => {
        if (file) {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (e: any) => {
            onChange(e?.target.result);
          };
        }
        return false;
      }}
    >
      {value ? (
        <Img width={width} height={height} src={value} alt="st" />
      ) : (
        <FileImageOutlined style={{ fontSize: '30px', color: '#2AC17E' }} />
      )}
    </CustomUpload>
  );
};

export default Upload;
