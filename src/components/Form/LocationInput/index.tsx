import React, { useState } from 'react';
import { Form, Spin, Tooltip } from 'antd';
import { AimOutlined, Input, Notification } from 'farmhub-ui-core';
import { EntityApi } from 'api/entity';
import { removeVietnameseTones } from 'utils/function';
import location from 'commons/location.json';
import { env } from 'env';
import { GoogleApi } from 'api/google';

const initProvince = {
  province_code: '0',
  province_name: 'Tất cả',
  districtInfo: [],
};

const initDistrict = {
  district_code: '0',
  district_name: 'Tất cả',
  province_code: '0',
  province_name: 'Tất cả',
};

interface LocationInputProps {
  name?: string;
  form: any;
  placeholder?: string;
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
  prefix?: React.ReactNode;
}

const LocationInput = ({
  name,
  form,
  placeholder,
  prefix,
  ...rest
}: LocationInputProps) => {
  const [isLoadingAddress, setIsLoadingAddress] = useState(false);

  const handleGetCurrentLocation = () => {
    setIsLoadingAddress(true);
    if (typeof window !== 'undefined' && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(position => {
        const option = {
          latlng: `${position.coords.latitude},${position.coords.longitude}`,
          key: env.google.apiKey,
          language: 'vi',
        };
        try {
          GoogleApi.getLocationInfo(option).then((res: any) => {
            const indexFirstWhiteSpace =
              res.plus_code.compound_code?.indexOf(' ');
            const originAddress = res.plus_code.compound_code?.substring(
              indexFirstWhiteSpace,
              res.plus_code.compound_code?.length,
            );
            const address = removeVietnameseTones(originAddress);
            let currentProvince: any = initProvince;
            let currentDistrict: any = initDistrict;

            location.forEach(province => {
              let prefixProvince = '';

              if (province.province_name.includes('Tỉnh ')) {
                prefixProvince = 'Tỉnh ';
              } else {
                prefixProvince = 'Thành phố ';
              }
              if (
                address.includes(
                  removeVietnameseTones(
                    province.province_name.replace(prefixProvince, ''),
                  ),
                )
              ) {
                currentProvince = province;
                currentProvince.districtInfo.forEach(district => {
                  let prefixDistrict = '';
                  if (district.district_name.includes('Huyện ')) {
                    prefixDistrict = 'Huyện ';
                  } else if (district.district_name.includes('Thị xã ')) {
                    prefixDistrict = 'Thị xã ';
                  } else {
                    prefixDistrict = 'Quận ';
                  }
                  if (
                    address.includes(
                      removeVietnameseTones(
                        district.district_name.replace(prefixDistrict, ''),
                      ),
                    )
                  ) {
                    currentDistrict = district;
                  }
                });

                const resultAddress = `${currentDistrict.district_name}, ${currentDistrict.province_name}`;

                form.setFieldsValue({ address: resultAddress });
                EntityApi.getLocationID(
                  currentDistrict.province_code,
                  currentDistrict.district_code,
                ).then(resp => {
                  form.setFieldsValue({
                    locationID: resp[0].id,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                  });
                });
                setIsLoadingAddress(false);
              }
            });
          });
        } catch (error: any) {
          setIsLoadingAddress(false);
          Notification('error', 'Không lấy được vị trí');
        }
      });
    } else {
      setIsLoadingAddress(false);
      Notification('error', 'Thiết bị không hỗ trợ định vị');
    }
  };

  return (
    <Form.Item style={{ marginBottom: 0 }} name={name}>
      <Input
        {...rest}
        prefix={prefix}
        placeholder={placeholder}
        suffix={
          <Tooltip title="Lấy vị trí">
            {!isLoadingAddress ? (
              <AimOutlined
                onClick={handleGetCurrentLocation}
                style={{ color: '#39B54A', fontSize: '20px' }}
              />
            ) : (
              <Spin style={{ color: '#39B54A', fontSize: '20px' }} />
            )}
          </Tooltip>
        }
      />
    </Form.Item>
  );
};

export default LocationInput;
