import React from 'react';
import { IconProps, Select, SelectOption } from 'farmhub-ui-core';
import { CaretDownOutlined } from 'farmhub-ui-core';
import { filterOption } from 'utils/function';

interface CSelectProps {
  placeholder?: string;
  icon?: IconProps;
  containerId?: string;
  value?: any;
  width?: string;
  height?: string;
  styles?: any;
  onChange?: (value: any) => void;
  color?: string;
  weight?: string;
  disabled?: boolean;
  defaultValue?: string;
  showSearch?: boolean;
  filterOption?: any;
  list: Array<any>;
  notFoundContent?: React.ReactNode;
  allowClear?: boolean;
  dropdownMatchSelectWidth?: boolean;
}

const CSelect = ({ list, icon, height = '50px', ...props }: CSelectProps) => (
  <Select
    showSearch
    filterOption={(input: string, option: any) => filterOption(input, option)}
    height={height}
    icon={{ children: <CaretDownOutlined /> }}
    {...props}
  >
    {list.length > 0 &&
      list.map((item: any, index: number) => (
        <SelectOption key={index} value={item.value}>
          {item.name}
        </SelectOption>
      ))}
  </Select>
);

export default CSelect;
