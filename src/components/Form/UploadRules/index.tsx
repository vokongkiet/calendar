import { Row, Space, StarFilled, Text } from 'farmhub-ui-core';

interface UploadRulesProps {
  isHaveAvatar?: boolean;
  isHaveVideo?: boolean;
}

const UploadRules = ({
  isHaveAvatar = true,
  isHaveVideo = true,
}: UploadRulesProps): JSX.Element => (
  <>
    {isHaveAvatar && (
      <Row alignItems="center">
        <StarFilled style={{ color: '#FF6347' }} />
        <Space width={6} />
        <Text size="xxs">
          Ảnh: Dung lượng tối đa: 2MB, dài: 720px, rộng: 720px.
        </Text>
      </Row>
    )}
    <Space height={12} />
    {isHaveVideo && (
      <Row alignItems="center">
        <StarFilled style={{ color: '#FF6347' }} />
        <Space width={6} />
        <Text size="xxs">
          Video: Dung lượng tối đa 15MB, thời lượng tối đa: 1 phút.
        </Text>
      </Row>
    )}
  </>
);

export default UploadRules;
