import React from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import styled from 'styled-components';

const CReactQuill = styled(ReactQuill)`
  .ql-snow {
    border-radius: 6px !important;
  }
`;

var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'], // toggled buttons
  ['blockquote', 'code-block'],
  [{ header: 1 }, { header: 2 }], // custom button values
  [{ list: 'ordered' }, { list: 'bullet' }],
  [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
  [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
  [{ direction: 'rtl' }], // text direction

  [{ size: ['small', false, 'large', 'huge'] }], // custom dropdown
  [{ header: [1, 2, 3, 4, 5, 6, false] }],

  [{ color: [] }, { background: [] }], // dropdown with defaults from theme
  [{ font: [] }],
  [{ align: [] }],
  ['link', 'image', 'video'],
  ['clean'], // remove formatting button
];

const modules = {
  toolbar: toolbarOptions,
};

const formats = [
  'background',
  'bold',
  'color',
  'font',
  'code',
  'italic',
  'link',
  'size',
  'strike',
  'script',
  'underline',
  'blockquote',
  'header',
  'list',
  'align',
  'indent',
  'direction',
  'code-block',
  'image',
  'video',
];

const TextEditor = ({
  readOnly = false,
  value,
  onChange,
  placeholder,
}: TextEditorProps): JSX.Element => (
  <>
    <CReactQuill
      // theme="snow"
      value={value || ''}
      modules={modules}
      formats={formats}
      onChange={onChange}
      style={{ height: 240, marginBottom: 40 }}
      placeholder={placeholder}
      readOnly={readOnly}
    />
  </>
);

export default TextEditor;

export interface TextEditorProps {
  readOnly?: boolean;
  value?: string;
  placeholder?: string;
  onChange?: (e: any) => void;
}
