import React from 'react';
import { CaretDownOutlined } from 'farmhub-ui-core';
import { Dropdown, Icon, Spacing } from 'farmhub-ui-core';

import {
  BoxColorIcon,
  FarmerColorIcon,
  PeopleLinkColorIcon,
  ShopColorIcon,
  TruckColorIcon,
  UserColorIcon,
  VolunteerColorIcon,
} from 'components/Icon';
import { Action, NameText } from 'components';

interface MenuSelectionProps {
  entityType: string;
  setEntityType: (e: string) => void;
}

const MenuSelection = ({
  entityType,
  setEntityType,
}: MenuSelectionProps): JSX.Element => {
  const getDropdownItemProps = (icon: React.ReactNode, pathname: string) => ({
    icon: {
      children: <Icon>{icon}</Icon>,
    },
    key: pathname,
  });

  const dropdownItems = [
    <Action
      onClick={() => setEntityType('all')}
      dropdownItemProps={getDropdownItemProps(
        <UserColorIcon width={18} height={18} />,
        'all',
      )}
    >
      Tất cả tài khoản
    </Action>,
    <Action
      onClick={() => setEntityType('farm')}
      dropdownItemProps={getDropdownItemProps(
        <FarmerColorIcon width={18} height={18} />,
        'farm',
      )}
    >
      Tài khoản nông trại
    </Action>,
    <Action
      onClick={() => setEntityType('store')}
      dropdownItemProps={getDropdownItemProps(
        <ShopColorIcon width={18} height={18} />,
        'store',
      )}
    >
      Tài khoản cửa hàng
    </Action>,
    <Action
      onClick={() => setEntityType('transport')}
      dropdownItemProps={getDropdownItemProps(
        <TruckColorIcon width={18} height={18} />,
        'transport',
      )}
    >
      Tài khoản vận chuyển
    </Action>,
    <Action
      onClick={() => setEntityType('warehouse')}
      dropdownItemProps={getDropdownItemProps(
        <BoxColorIcon width={18} height={18} />,
        'warehouse',
      )}
    >
      Tài khoản kho chứa
    </Action>,
    <Action
      onClick={() => setEntityType('collaborator')}
      dropdownItemProps={getDropdownItemProps(
        <PeopleLinkColorIcon width={18} height={18} />,
        'collaborator',
      )}
    >
      Tài khoản cộng tác viên
    </Action>,
    <Action
      onClick={() => setEntityType('volunteer')}
      dropdownItemProps={getDropdownItemProps(
        <VolunteerColorIcon width={18} height={18} />,
        'volunteer',
      )}
    >
      Tài khoản thiện nguyện
    </Action>,
  ];

  const currentAccount = () => {
    switch (entityType) {
      case 'all':
        return (
          <>
            <UserColorIcon width={18} height={18} />
            <NameText>Tất cả tài khoản</NameText>
          </>
        );
      case 'farm':
        return (
          <>
            <FarmerColorIcon width={18} height={18} />
            <NameText>Tài khoản nông trại</NameText>
          </>
        );
      case 'store':
        return (
          <>
            <ShopColorIcon width={18} height={18} />
            <NameText>Tài khoản cửa hàng</NameText>
          </>
        );
      case 'collaborator':
        return (
          <>
            <PeopleLinkColorIcon width={18} height={18} />
            <NameText>Tài khoản cộng tác viên</NameText>
          </>
        );
      case 'transport':
        return (
          <>
            <TruckColorIcon width={18} height={18} />
            <NameText>Tài khoản vận chuyển</NameText>
          </>
        );
      case 'warehouse':
        return (
          <>
            <BoxColorIcon width={18} height={18} />
            <NameText>Tài khoản kho chứa</NameText>
          </>
        );
      case 'volunteer':
        return (
          <>
            <VolunteerColorIcon width={18} height={18} />
            <NameText>Tài khoản thiện nguyện</NameText>
          </>
        );
    }
  };

  return (
    <Dropdown arrow items={dropdownItems} containerId="detail_account">
      <Spacing styles={{ cursor: 'pointer' }}>
        {currentAccount()}
        <CaretDownOutlined />
      </Spacing>
    </Dropdown>
  );
};

export default MenuSelection;
