import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { NameText } from 'components';

import { Row, Space, Spacing, Button, ButtonType } from 'farmhub-ui-core';
import { PlusIcon } from 'components/Icon';

interface PageTitleBarProps {
  title?: string;
  description?: string;
  url?: any;
  ExtraAction?: any;
  bottom?: string;
  padding?: number;
  logo?: JSX.Element;
  background?: string;
  radius?: string;
  breadcrumb?: React.ReactNode;
}

const PageTitleBar = ({
  title,
  url,
  ExtraAction,
  bottom,
  padding,
  logo,
  background,
  radius,
  breadcrumb,
}: PageTitleBarProps): JSX.Element => (
  <Wrapper
    radius={radius}
    bottom={bottom}
    padding={padding}
    background={background}
  >
    <Row>
      {logo}
      {logo && <Space width={24} />}
      {breadcrumb}
      <NameText lineHeight="auto" size="md">
        {title}
      </NameText>
    </Row>
    <Spacing size="middle">
      {url && (
        <Link to={url}>
          <Button
            height="42px"
            type={ButtonType.Primary}
            icon={{ children: <PlusIcon width={12} height={12} /> }}
          >
            Thêm mới
          </Button>
        </Link>
      )}
      {ExtraAction}
    </Spacing>
  </Wrapper>
);

export default PageTitleBar;

const Wrapper = styled.div<{
  bottom?: string;
  padding?: number;
  background?: string;
  radius?: string;
}>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: auto;
  background: ${p => p.background || '#fff'};
  padding: ${props =>
    props.padding ? props.padding + 'px' : '0px 0px 18px 0px'};
  border-bottom: 1px solid ${p => p.theme.colors.border.light};
  border-radius: ${p => p.radius};
`;
