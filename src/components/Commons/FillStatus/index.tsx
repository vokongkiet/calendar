import React from 'react';
import { ICONS } from 'utils/constants';
import { Icon, Row, Space, Text } from 'farmhub-ui-core';
import { theme } from 'styled';

interface FillStatusProps {
  isFilled: boolean;
  name: string;
}

const FillStatus = ({ isFilled, name }: FillStatusProps) => (
  <Row>
    <Icon
      svgProps={{
        icon: ICONS.CHECK,
        color: isFilled ? theme.colors.primary.main : '#8c8c8c',
        width: '24px',
        height: '24px',
      }}
    />
    <Space width={12} />
    <Text color={isFilled ? 'main' : 'primary'} weight="bold">
      {name}
    </Text>
  </Row>
);

export default FillStatus;
