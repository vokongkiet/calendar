import React from 'react';
import {
  Tooltip,
  Popover,
  PopoverPlacement,
  TooltipPlacement,
} from 'farmhub-ui-core';
import styled from 'styled-components';

interface IconWrapperProps {
  displayBackground?: boolean;
  icon: JSX.Element;
  size?: number;
  background?: string;
  styles?: any;
  hoverBackground?: string;
  hoverColorIcon?: string;
  tooltip?: string;
  popover?: JSX.Element;
  onClick?: (e: any) => void;
}

const IconWrapper = ({
  icon,
  size,
  background,
  styles,
  hoverBackground,
  hoverColorIcon,
  displayBackground,
  tooltip,
  popover,
  onClick,
}: IconWrapperProps): JSX.Element => {
  const handleClick = (e: any) => {
    e.stopPropagation();
    onClick && onClick(e);
  };
  return (
    <>
      {!tooltip && !popover && (
        <Wrapper
          onClick={handleClick}
          size={size}
          background={background}
          hoverBackground={hoverBackground}
          displayBackground={displayBackground}
          hoverColorIcon={hoverColorIcon}
          styles={styles}
        >
          {icon}
        </Wrapper>
      )}
      {tooltip && (
        <Tooltip placement={TooltipPlacement.Top} title={tooltip}>
          <Wrapper
            onClick={handleClick}
            size={size}
            background={background}
            hoverBackground={hoverBackground}
            displayBackground={displayBackground}
            hoverColorIcon={hoverColorIcon}
            styles={styles}
          >
            {icon}
          </Wrapper>
        </Tooltip>
      )}
      {popover && (
        <Popover placement={PopoverPlacement.TopRight} content={popover}>
          <Wrapper
            onClick={handleClick}
            size={size}
            background={background}
            hoverBackground={hoverBackground}
            displayBackground={displayBackground}
            hoverColorIcon={hoverColorIcon}
            styles={styles}
          >
            {icon}
          </Wrapper>
        </Popover>
      )}
    </>
  );
};

export default IconWrapper;

const Wrapper = styled.div<{
  displayBackground?: boolean;
  size?: number;
  background?: string;
  hoverBackground?: string;
  hoverColorIcon?: string;
  styles?: any;
}>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${props => (props.size ? props.size : 52)}px;
  height: ${props => (props.size ? props.size : 52)}px;
  border-radius: 50%;
  background: ${props =>
    props.displayBackground
      ? props.background
        ? props.background
        : '#F8F8F8 !important'
      : 'none'};
  cursor: pointer;
  /* transition: background-color 0.2s; */
  /* 
  path {
    fill: ${props => props.theme.colors.text.secondary};
  } */

  &:hover {
    background: ${props =>
      props.displayBackground
        ? props.hoverBackground
          ? props.hoverBackground
          : 'rgba(57, 181, 74, 0.1) !important'
        : 'none'};

    > svg path {
      fill: ${props =>
        props?.hoverColorIcon
          ? props.hoverColorIcon
          : props.theme.colors.primary.main};
    }
  }
  ${p => p.styles};
`;
