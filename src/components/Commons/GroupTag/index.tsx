import { Badge, Tag } from 'antd';
import styled from 'styled-components';

const CTag = styled(Tag)<{ height?: string; margin?: number }>`
  position: relative;
  display: flex;
  height: ${p => p.height || '30px'};
  align-items: center;
  margin-bottom: ${p => p.margin}px;
  cursor: pointer;
  max-width: 260px;
  flex-shrink: 0;
  font-size: 14px;
  font-weight: ${props => props.theme.font.weight.medium};

  .text {
    white-space: pre-wrap;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    display: -webkit-box;
  }
`;

interface GroupTagProps {
  name?: string | React.ReactNode;
  color?: string;
  quantity?: number;
  icon?: React.ReactNode;
  margin?: number;
  onClick?: (e: any) => void;
  height?: string;
}

const GroupTag = ({
  name,
  quantity,
  icon,
  color,
  margin = 14,
  onClick,
  height,
}: GroupTagProps) => {
  return (
    <Badge offset={[-16, -2]} size="default" count={quantity}>
      <CTag
        color={color}
        onClick={onClick}
        icon={icon}
        height={height}
        margin={margin}
      >
        {name}
      </CTag>
    </Badge>
  );
};

export default GroupTag;
