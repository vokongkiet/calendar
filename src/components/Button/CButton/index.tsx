import React, { useEffect, useState } from 'react';
import { Button, ButtonProps } from 'farmhub-ui-core';

interface IProps extends ButtonProps {
  src?: any;
  rules?: Array<any>;
  children?: any;
}

const CButton = ({
  children,
  src,
  rules = [],
  loading,
  ...props
}: IProps): JSX.Element => {
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    const handleValidate = (src: any, rules: any) => {
      if (!src || rules.length === 0) {
        return false;
      }
      let isDisable = false;
      rules.forEach((e: any) => {
        if (e.field === 'any' && Object.keys(src).length !== 0) {
          return false;
        }

        e.conditions.forEach((condition: any) => {
          switch (condition.name) {
            case 'minLength':
              if (!src[e.field] || src[e.field].length < condition.value) {
                isDisable = true;
              }
              break;

            case 'any':
              if (!src[e.field] || src[e.field].length < condition.value) {
                isDisable = true;
              }
              break;

            default:
              if (!src[e.field]) {
                isDisable = true;
              }
              break;
          }
        });
      });

      return isDisable;
    };

    const isDisable = handleValidate(src, rules);
    setDisabled(isDisable);
  }, [src, rules]);

  return (
    <Button disabled={disabled} loading={loading} height="50px" {...props}>
      {children}
    </Button>
  );
};

export default CButton;
