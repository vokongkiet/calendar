import React from 'react';
import { IconProps } from './Icon';

export const InfoCircleFilledIcon = ({
  width,
  height,
  className,
  color,
}: IconProps) => (
  <svg
    className={className}
    width={width}
    height={height}
    color={color}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="24" height="24" fill="url(#patternInfoCircleFilledIcon)" />
    <defs>
      <pattern
        id="patternInfoCircleFilledIcon"
        patternContentUnits="objectBoundingBox"
        width="1"
        height="1"
      >
        <use href="#image0_305_482" transform="scale(0.015625)" />
      </pattern>
      <image
        id="image0_305_482"
        width="64"
        height="64"
        href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAABuwAAAbsBOuzj4gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAMtSURBVHic7ZsxaxtBEIW/ldTEbWIS5CJFmlTuUhgMgRhSGBuM1aQOuHYXcKM2kL8gSMCQIkWwwZ2NhQLGLvIHgo1AKWwIiTtjN5Imxa6s0+kkK5fb3TOrB4OkG7iZ97Q3t7c3q0QEm1BKzQCvgGdAGZgznz0DuIjYuflsAnURubaanw0BlFIPgVVgDXgNPEh5qhtgH9gF9kTkMpsMIxCRTAwoARtAA2gDkrG1zbk3gFJmeWdEvgKcWiA9yk6BincBgEXg2CHxuB0Di84FAGaBHY/E47YDzDoRAJgHWjkgHbcWMG9VAGAduMoB2VF2BaxnLgCggCrQzQHJu6xrclWZCGDIb+eA2L/a9iQiTCJANQdk0lr1vwRAX/P3YdiPsi531IS7qn2eC96kdsWYu0Pis4BSahb4DjwdcmaDLvARODS/l4C3QMFSvJ/ACxH5PeQZ8e/bnOR0gOWEmMvGZyvuzkSXAHp6a3NI1sZcdjXLsYemzUlD7kPCsSxxmNKXBYa4DQiglKoAC5aT8IkFw7GPyPAr4eaR1uclIIZjaagGoBcabAcX/BXBqG0kCdBwFLwnQg14Y6zmkLwAjYF5gFnD+wUUCQMd4LGIXPaK4CrhkAfNdRX6d4E1f7l4wxroR90Z4A/pl67T4gfwzXx/CTx3HP8GeASwgrvi07P3QDFSgIvmmOs8VgA2HQc9GDMPOHCcy2aB/uspV/iS0mcD5QL6XZ1LNFP6bGDOxwiQlD4bKPsQIE+YCmBrCereoIBuRggVF1MBmArAue8sPOJ8OgJwP/tyPdkZh2YBqKMfDV1BOYw1DjdAvSC6D2/fdzYesC8i172J0K7XVPxgF/pLYnvohcJQ0EFz1gKI7sA88pmRYxwZzgOvxj57SsYHbrlGBfgEnLnPxTnO0FyBiAAi0ga2fGTkGFuGKxB7OywiX4ETywk8SenLAieG4y2S1gPeWU5iKaUvCwxzC71FZtokFXqb3LRRcpzTiBBuq6wRIOxm6YgIYbbLJ9SEPBdGOxsmEu4OrRyQjVsL21tmIiKEu2kqJkSY2+YShAhz42RMhHu5dTb4zdNWBBgIkPPt838BvnZC2khKwlEAAAAASUVORK5CYII="
      />
    </defs>
  </svg>
);
