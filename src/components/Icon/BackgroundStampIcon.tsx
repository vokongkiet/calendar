import React from 'react';
import { IconProps } from './Icon';

export const BackgroundStampIcon = ({
  width,
  height,
  className,
  color,
}: IconProps) => (
  <svg
    width={width}
    height={height}
    className={className}
    color={color}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="24" height="24" fill="url(#patternBackgroundStampIcon)" />
    <defs>
      <pattern
        id="patternBackgroundStampIcon"
        patternContentUnits="objectBoundingBox"
        width="1"
        height="1"
      >
        <use href="#image0_308_1239" transform="scale(0.015625)" />
      </pattern>
      <image
        id="image0_308_1239"
        width="64"
        height="64"
        href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB2AAAAdgB+lymcgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAkSSURBVHic1Zt/jFXFFcc/+xYX8McuEKmiRZG6i7gFSbW4rT8i1OC2MW1EMU0TE2irFuOvqpGihuAfJv7ghz9WGmqsxlYKC7VoLFZFWzAxSqut1LZ0qb+wRZFlxbp0l4Xd1z/Ove7cMzP3zn3v7a58k0nee+fMmXPunTlzzpl5MHRoBt4H3gVmDqEeQ4KvAp1AMWr7gKYh1WgQMRH4kH7j4/YRUD+Eeg0KqoE/Yxsft9cjnkMGk4GNwCbgFqAxoM84YCVwkH7DDwArIloWpgCLgJeBF4FJubWuIP6A/RbbgLuArwGFlL6zgA5gD+lOsAqYHsnc7hjvuXIMKAcjgR6HQmb7D9ACTPXIaAC+5KE1AsuBHRljdAHDy7amBJyVoZhum5C3noVZwAtAXw7ZQ7KD3JhDQbOtBGoc8oYDj5Qo84YBsC8TrSUqWwRWIWs7RgFYW4a8tQNmZQqy1mZWu8KQdVWZsnaUasSwEvsdC4wvoV87sAUJf80Z0ItsgxMQj390TrnjgeMRpzso+Dbhb+dNYAESM1S5hClUAacCPwH+lmOc2RWxLBALAxT6PXBOBcY6F3e8odvCCoyVwJHADOA24EpF+2mKIu8B36y0MsC3SPc7KxX/fCRivACoyzPQJGAzyVD1AcXzW48SjwG1Hrk1yANdDKxDfMH2qG1BPPli4DzgMI+MOuCXnrE3KN4VBq0XeAU4xWe0iacdwu9RPL9T9F5kzbpwInAvsNujuKvtRqLAEzwyF2IHSk8rnvsccp9KsfszPOPouErxPGTQ+oDLHXJqEcOzwuW01oM8CNesupzkQ3hQ0dc55D2ZaT1wnaPjG4rnewbtJoeMJmSrC3nTITPjHWR71LjZ4LlE0f7ukDM/y3iQKRs/2T7gJWCu4jkc2AX8ytF/DtDtGLwHmV1XIgmQmcAMB04GfoQsL9es6QYudoy3GvgAGKF+vwzZPUxbjvdarfAakm9PS+GZjT0155B0nvHAq/FnfS6cDKzBXucHsR9CLXBRiqypiC1bcozPKaTn8i40Yb/5PcD5OeWYiGsGeia4lkMaClSgcDIqUuiHSCpsohZ7zbchb9KFkcDXkVk0GymejPTwNmAXQt4GjlJ8ZyGO8QJgTJhJYahH1vp+Q4FvKJ57sd+8y/hpyH6/D3uN74toruJJA/ZMWKJ4zjdoPcguMDnYSg/mOpTVO8KJJJ1WH/a0rwaWIfFClsfvReIOXRydRdIn7CeZkFUBW5WsLgI9v0YN8LBHwQWKV7/91YpejQQgeWOA9dgPQdcMlim6L0dpRXauIByBOxiKm+lMakju4z3YU3+ZQ0Y7ErtfE7WfIctG8+kItJ7kbNtFMqWfnKL3q8DYLOOPjhh9Qv6t+Gcouo7Hp2FP+0dxJyd1SC6ht70piu9ZxaMzzp0p+m8DTvIZPxH4R0rnIna8vVjRddaow9FHfYMb0A9hjaLPV/RFip42e4tI0PQVPWgTcjSVtS7vyjDQDHYOJ+lA2wlLS0eRXA6dJLfIejVmq+p/T4Ad/yVy1AUkd3+RgPWBbEUmJhif9RI5jaTj+TXwifF9KpJ2byI5zfcCTxjfj4hkxXg/GsulA8jDy8JRSEp/UQFxQL4gROMT9d0MRvYgW1MMHXf/VX1vQdbvudFnE1vVd1NWN8kXocPxvYShBmgpIOdylYCu9xVz0DWvDsP7MuilorMAzEPWRAj0Gv7U+DyGZIa3U/Fqb341/UvgmgzeD4zPIxA/EUPrPoow7EVsB2Q9xmsrrel9WTtBMwYYSfISxB7CnOBokmHvpyRT3QbSneCSADt2RjZ/NpW2IjuBDnM19PH3m+q7GQJ3IVtSjDHYtUUXHkAeQowNyLp3jQG2b/lyhvw2JHHSfgYQh/ICpQdCzyj6adiB0GO4p+lo7ILnQYdBzymesxU9LRDaQsBuVwM8niLErKweRjJ+cIXCrinZgdQVr43aQ9jZXhG4U8lqQJx2TP+QZCjcmKL3RuwU2osCcAfuI2qdDC1XdB25VSOJTda61O0J7GRI+5yliu5Lhlrwl9pT0YwdIeq1cwJ2OqzvAVQjDjQkHT6IvHltfDN2OvxFg16F+CRT1l7sYmlujEWedLshWBdE9CzoQKarxhRkhnRiG94Z0VxObBLwseK/W/GYBZHdSBZ6bLCVARgWGdCMvU/XIqVrU8HtuB8CyBbZRH9JrAm7qhtjEvAvJfst5OjOxBSkHNZIBW6anZmikA/TsYuiHZFSpaIZ+813AWfklDOCnNdo3gD+SfqdnsuwA5tLcJfF15LvEmQD4vBcZXF9DF4X6eJCFXKo2oYctQdhohp0G/bpTx3yZnT5C6Ru7zsYeRbJ5+tJzrARiNFXIfv8AUf/Ltx3ANZEuuikaAGyVEwZviWZwA2Owf+ieOYaNNeh6HSkdJ3l8dtJOldfewv3tL/F4JmnaK875Ogt3IkNjo76cNQsmPZhV4JAgo4lJMvpedt+ZPvUDg/kGM3kfVjRf+GQtzHTety3tZYrHl2X60MuUriuwIxHtqRdgUYXkQhvKcl9PkYV8iZ1TKHrkUsdctdlWh8N+hskzYw7rlA8vrrbKvzp6DCkALIIyeBeRZxTW/S5NaKdg//y1ijkkMY19nrF22LQOhB/dVyK3Raqkb31CuQ43MRKjxJFJK2+MM9AgbgQScZ8496v+L8LfJ/wy1m5EHJJajOSLZaLmZGsrPEG9bbodwIUits24FZkNoWUsgoR761ILBI6Tsg9ZAulTo1jEEeVFx/Tf1Hyj/R77h8gf6OZgGyhox1901BEDnV01XpAEbLPp7U5hqw5ZcpqGzArU5BWNMlqrtKY61ZXaHt8AOzLxNUlKHoAuB330quKaDqXCGnXDoB9mTg9h4LdSC3Q988RE9OQPTvP9bq8V2YqgmG4Cxtx60NuZ94EfMEj43Qk9XZhHBLrv5YyRhFJklx/wBgUPKmU6UX27Ovx3/KMcSlyeNqNfQ1PYwLwY+QQRS8RHQIPKo5D/uayHokYjwnoMxkJe3Wu/zzp1/JijEUivFbg5+S4+/d5QDUSB/im858o/U8chwzGIVfqtfE7Ke1fKIckGpGSdWz8/xgiTz6UmIFUkndQXuG0LPwf20BQc7r7LnoAAAAASUVORK5CYII="
      />
    </defs>
  </svg>
);
