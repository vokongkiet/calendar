import React from 'react';
import { IconProps } from './Icon';

export const DeliveryIcon = ({
  width,
  height,
  className,
  color,
}: IconProps) => (
  <svg
    width={width}
    height={height}
    className={className}
    color={color}
    viewBox="0 0 64 64"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="64" height="64" fill="url(#pattern0)" />
    <defs>
      <pattern
        id="pattern0"
        patternContentUnits="objectBoundingBox"
        width="1"
        height="1"
      >
        <use href="#image0_101_9" transform="scale(0.015625)" />
      </pattern>
      <image
        id="image0_101_9"
        width="64"
        height="64"
        href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB2AAAAdgB+lymcgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAANtSURBVHic7do7jFRVHMfxz64P8JFIIiRiow26Ma4NVgY6oOFhQWgoLCyIHcbG2KmVjSZCo5WJUhgtNbGggAKtlIrGaBBXCQmQGI1ERdm1ODPJOHvnnvs4554xmW9ymtmZ8//9f+fxv3vPoQw78CbWRu0NbC+kZVAew7u4hY2p9ic+xK5i6jKyKiR32+bEp9sdfIZniyhNzB4hmXXxxKvaBRweXHVPlvE8vlKf3BpeHrW1yHe/xBEsDZhHa+7BC7ikPpnvcBJbE/22OA8Ion5UL/4bIcm7avpaEqb8hUhf1/A6HkqdTBu2j0TclGcdN9k/fhWqys7uabTncZxWXcrGbR1nhN2/L6v4CH/XxLuFU0KZzUabUraBb/EqtiWKX/cMkbWE9i1lv+F9PJVIT+6lh/al7EVcjHz3Dr7AwVH/fXlQhhLatxzt1myZfC8sj4dbJp1DM9KWMnhESPCnSH9/CIY90zLpKjqX0APq19M6PsfeDqLuxXHxpbSBcziKuzvEmWavoLlu37qB/fBDzZc+laaUEZbHB8Kox/aV14R/mfuyKuQwK9ZlETG/CDt4KhMIZfEkrkRi/4VP8FzHOE/gLVyPxIlOzXH7Gidwf0dB0ywLa/aseIkdx74v0ucWHGvY50wDYj+8ibex0if7KZ7Ge/g9Evua8Cbp0anfr4w0xZ4NqnLb9EGbzs4Lm9yWREZswytCmayLfRsf46WRhjaDFjVgTJvplHqvWMY+YQ/4JxK77bJtbMAkJWfFLmF5NF3TsSXayYAxQ8+Kpjv5rNGuopcBucWR3+RkBqQWnMvQaZIbMEmXveK49jt5H7IaMKbLA0mO0a5iEAMmaTO1U26gsxjcgDFNZkXq0a6imAGTVD2RpXy0ruM/cZdsTnqo05e5iJvi3dz/moUBpQWUZmFAaQGlWRhQWkBpFgaUFlCahQGlBZRmYUBpAaVZGFBaQGkWBpQWUJoqA4Z6NVWCytzm5uVkJmIvY12t+HDyMOIdeWZFbgNWBO11hzQ/wyHxd/aTJzmpbmrnMGCr5idN14U7iyhzFyClAUnPFYsdUrYk+6ANfkzdkKEGKHvQNgaUvK6TTUgTA4qMdlP6iptlwNyMdlO6Cp7+21yPdlOe1P3CYokLmtmY5xsigzNvN0SKMS83ROaC8V5xddSKre1/Ad/GBbOjFz63AAAAAElFTkSuQmCC"
      />
    </defs>
  </svg>
);
