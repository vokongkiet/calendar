import * as React from 'react';

// import { theme } from '@src/styled';

import { IconProps } from './Icon';

export const PendingIcon = ({ width }: IconProps): JSX.Element => {
  const DEFAULT_WIDTH = '64';

  return (
    <svg
      width={width || DEFAULT_WIDTH}
      height={width || DEFAULT_WIDTH}
      viewBox="0 0 64 64"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <rect width="64" height="64" fill="url(#pendingIcon)" />
      <defs>
        <pattern
          id="pendingIcon"
          patternContentUnits="objectBoundingBox"
          width="1"
          height="1"
        >
          <use xlinkHref="#image0_101:4" transform="scale(0.015625)" />
        </pattern>
        <image
          id="image0_101:4"
          width="64"
          height="64"
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABAEAYAAAD6+a2dAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAABgAAAAYADwa0LPAAAAB3RJTUUH5QoWATAChRUNmwAACK1JREFUeNrtnXtQVPcVx7/n3lXwDeIrmaiND1AXSZrOqLUVXeMjpmoNwUcaq2KLKVaqYtWIQx0SK40lJoARFcQ2diYNKvhMx46IxWrjJJNGwwIbFQXTipGXr8aQ3Xv6x7oLhVjv3ddvZe/nH2aY+7u/c37n3HPv/f1+97uAjo6Ojo6Ojo5OwEGiDfA0rHy2DujbFyT9Rj4fGQnQ1yRFRADci2sGDgQoHLkhIQBu0o+7dm3RtAcfvHMH4M/x88ZGgGqpX1UVwEGsWCxg5TVbVGkpSSPTgOvXRfvpKR65BGBmBiQJXPG0fPTZZ0HKUBx54QUwKRhuMoH4dUoaNsx7FtCPOLO8HOAbsBQXA6jFzwoKgBHBtqjiYiIiQFFEj5Nqb0Qb8DBYubAM6N0b9M0YuTExEcAfKTMuDuC38doTT4i2rxmagNevXgUwjlfu3g2Wp9iCs7JIivgFUFsr2roHWi3agNYwnz8PhIaCDR/IOSkpADZQ9pIlIP4E57t0EW2fekfoGUTdvQvwdV6zfTuoqYdt3saNRN8loLFRtHkOhCeAvaQTAeZq6eCCBYD0U+nI5s0AZ2N3nz6i7fMc9APEX78Oxp+UuatXkzRiujJhzx7hVonq2F7au3cHNQ03/D4nB8AErJszR/SA+JAiLDxwALBNsO5avJgoKgpoaPC1ET5PAOaymA53jEaAJSX18GEAqfTWk0/62g6/gWkNr66sBCksn54+nSjyg6aS8nJfde+zBGDFHGsoGjUKQBTyjh4F4UW836uXr/r3f2g0pjU0AMpb6DRjBlFkvDX/9Glv9yp5uwPmzy4YEsaOBdFlrCgu9rvAM5iVOXOa/9IrvGvLFgGGnMVfQkPB0jJcPHaMuexzQ+aYMd7u1WsVoEWpT+cFJSUAvsKhnj297ZDmASCj0Wol5ziwYi6VpdmzQSCS8vMFmnYRMXV1APZIJdHRRMZ9TTVlZZ7uxOMVgPmfDISEtLjH+2XgHwGGoCAsDEz/sMUfOsTKpYtAjx6e7sTztwAOyjCs2LULgf5w5ymI/0ppgweDvh5jSMnN9fTpPZYAzKVV0sGFC0E8GVtjYnw7SoEA/w1psbHM5o+lT19+2VNndTsBnDN3zgkcHe9Ck6Qdb77ZfKt1D/crgHPKtr3N3PkrfBo5ffuCO86WjyYnu3s2lxOAuTwLCAsD8YuUEx8velgCDzpMKUuXsmLZDrj+Wu1GBeBQuXH5cgBN+OR/1tV1fIFjcYysr8i8bJmrp9GcAM71eGAlZS9aJHocdHCNtsTFtYiLJrRXAMdGDPBJpPTvL9p7HTRg7YABgHm/3Dh+vNbG2hPAsQNHx8+gaGRoj4trzwBDJ04U7a5OG/IwyGTS2kh1AjCbY4F+/QCk0pqICNHe6rRhEMUbjaycWwWofx1XXwGYTPKpyEjRXuo8ECOsRCD5+3Kj0ai2kfoEIOUl6qBf+f6PlExW9XHSUgGWcN3AgaLd03kYXMmfql+E01AB8Dbe795dtHseg+ky+qekiDbDC8zC/m7d1B6s5S3gLs1qFzN+FoxOTiZpxHTr5Y0bHf+0b1INCgJwj0bFxYk20g1+S5PVX6he3xLmN9y/4omMMdbTaWnOfyvmWKBjR9A3lwyz9u4FoRM+njZNtLlucALBzGoP1pIAXfjAnTuivdOMI/BtrnhH4OmuYda+fQCn48iMGaLN9QBFfPj2bbUHa3gIxArMvXVLtHca+P+lnvBDQ3RhYTsK/H3IhJ+oj5OGh0DeSWFVVaLdeyhqSz0wGWeef160uV4gg4ZfuaL2YA23AOrEnSsqRHv3QAKv1D8Am4XD1MdJwy3A/n08ADMM6h8yfECAlvo22OPCwYk2MpvVNlKdAE5hBKY/82aLRbS3eqlvQyXnmM0kDd0K3LihtpELy8F8DJdOnBDoqH7Ffzt98YX2uLgyD3BfEcPX0GyO3bmz7RXvCHzTEEN0QQEC54pvhbIIrxYWam3lQgLYpVBaKGL4ik5knj+fuXSf3GgyBXCpb00o3qiuBiJho5ISrY01J0ALDRy7FIrP4HdR3rkzQE/RlEOHQFgp3z1+PABLfWv+jvV5ea5qE7kxFUwmW3BmJoCOeManM4SOXcgh1GPcOB/26184JGjYUG+99847rp7G5QQgGp4I1NWB0ZuTduwQPR4BB2ELZ2zd6q4Ildufh7eQeplqWFVRAWAQMh57TPT4tGO6YWlNDTi4yZo5bBhJg4cAN2+6ejK3VwPt7523boGJlBlr14oenfYPf6UkJSW5G3gHHlsObla9ok3Ife89sYPULjmJtPx8osgI5TueG19v6AOkWhclJDjFj3Tc5SqnXrgA7lhuXe35bzA9ngAtStPTctJzzwGUgLgvv/TJULUnGPsxt7YWUJLlqpkznbdaD+N1lTCn2BFjJP5w/Pgjp/jpe+yv1UwTsHLSJJJG/Mv60tmz3urM61vCiEaEW3/14YeAsgHvmkwANmC++sWKAKITZtbXAwrwvalTvR14Bz7bE0hSZLI18qOPAP6PVDl+PJim8LpLl3zVvx9jv8dDPiFFjx1LNHKoNfvMGV91LlAqtiIX6NYNZFtr2LFzJ4AS/HLePFH2CGAD4gsLgaa91uzFi0WJSAsXi3bQQvyoj5SXng7gNrb16yfaLg9SieXXrgEcriSuWuXp1zlX8ZsEcNCsh3fvVfno+vUgpFFqQgIePSUSC0bfvg3QNf7dtm3gDuW2cZs2eetp3lX8LgFa49QigrJM5sRE3FfEgFMYwW+4vyxrX50DkG+9l5VFZNwH1NeLNu5B+H0CtKZZCsWhiOEURjiHARMnAnic4tV/HasBx17IUM4uKwPwBWqKigCpiN4oKACG7bE2njql/2SMYJp/YqapRr4yciRA56lreDiYz/G/BwwAoQ7pPXui7adu9g9fGGH4dX09iJ6ix6urAWUTGywWcNBJW0hpqdY9dzo6Ojo6Ojo6Ojp+x38BnQK6n6ajFGwAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMTAtMjJUMDE6NDg6MDIrMDA6MDDESHcKAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTEwLTIyVDAxOjQ4OjAyKzAwOjAwtRXPtgAAAABJRU5ErkJggg=="
        />
      </defs>
    </svg>
  );
};
