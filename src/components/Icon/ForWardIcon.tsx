import React from 'react';
import { IconProps } from './Icon';

export const ForWardIcon = ({ width, height, className, color }: IconProps) => (
  <svg
    width={width}
    height={height}
    className={className}
    color={color}
    fill={color}
    viewBox="0 0 16 16"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fill={color}
      d="M16 7.9l-6-4.9v3c-0.5 0-1.1 0-2 0-8 0-8 8-8 8s1-4 7.8-4c1.1 0 1.8 0 2.2 0v2.9l6-5z"
    ></path>
  </svg>
);
