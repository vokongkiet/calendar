import React from 'react';
import { IconProps } from './Icon';

export const DownloadIcon = ({
  width,
  height,
  className,
  color,
}: IconProps) => (
  <svg
    width={width}
    height={height}
    className={className}
    color={color}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="24" height="24" fill="url(#patternDownloadIcon)" />
    <defs>
      <pattern
        id="patternDownloadIcon"
        patternContentUnits="objectBoundingBox"
        width="1"
        height="1"
      >
        <use href="#image0_308_1240" transform="scale(0.015625)" />
      </pattern>
      <image
        id="image0_308_1240"
        width="64"
        height="64"
        href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB2AAAAdgB+lymcgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIbSURBVHic7Zo9TsNAEIU/ECKIjtwBOAe0gMSdECLUcAE4AgWigRNwDRJExV8PRWwJjL07ux52DJlPGik/np33nhM5sQ2O4xiwDpwBT1WdVa8tDOfAR6POTRUVZsbPAB4thCxZDGVuuI3iepZLDxwaHoC1AGs8AGsB1ngA1gKs8QCsBVjjAVgLsMYDsBZgjQdgLcAaD8BagDUegLUAazwAawHWeACN5//tis0qcApMgQdgUr3WSakrNs0ZdWkzaZkxCTX0vWIzBnaBrch2OQFsV2uPE/RMW2bMtIXVHAIvX3ougZHCnFG1Vr3NczVLQrKf3ADGfDdf1zXtIUjnjKo1mts9AxsCXcUC2A30toUgmdNlvq4dga5iAWwFettCiM2Jmf8ANgW6igUA37+nsRBCcyTmL4SaigawClxFhN8Aa4H3U9aQUDQAkO290PuS3q4ji4qfvgGAbC/mVMqez/ajEQDoh5BjPsuPVgCgF0KueQJr6jVE6BtCH/ME1tVrEJAbQl/zBNbWaxCSGoKGeQLr6zUkIA1ByzyBGXoNicRC0DRPYI5eQwZdIWibp2XGIAKA+a+5E+YnLKbV45RfeFKifpq3pnaZtbqlti9RP35W2FqANR6AtQBrFj6AFeF2v3EoHATNT8C7iYqyvH590gzgvqAQK4IeD0j/2/rXai+W0PEARP5WHcXM1+wDd8DbAET3rTfgFsGed5wF5BOM47WMwPENIgAAAABJRU5ErkJggg=="
      />
    </defs>
  </svg>
);
