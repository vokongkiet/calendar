import React from 'react';
import styled, { css } from 'styled-components';

import {
  Text,
  Space,
  Button,
  ButtonType,
  Spacing,
  Modal,
  BellFilled,
} from 'farmhub-ui-core';

import { NameText, TitleText } from 'components';

interface ConfirmModalProps {
  isVisible: boolean;
  setIsVisible: (e: boolean) => void;
  onOke?: () => void;
  onCancel?: () => void;
  title?: string;
  message?: string;
  name?: string;
  isLoading?: boolean;
  okText?: string;
}

const ConfirmModal = ({
  isVisible,
  setIsVisible,
  onOke,
  onCancel,
  title,
  message,
  name,
  isLoading,
  okText,
}: ConfirmModalProps): JSX.Element => {
  return (
    <Wrapper onClick={(e: any) => e.stopPropagation()}>
      <Modal
        maskClosable={false}
        centered
        bodyHeight="auto"
        styles={ModalStyles}
        bodyPadding="0px"
        visible={isVisible}
        onCancel={() => {
          onCancel && onCancel();
          setIsVisible(false);
        }}
      >
        <WrapperContent>
          <WrapperIcon>
            <BellFilled style={{ fontSize: 54 }} />
          </WrapperIcon>
          <Space height={24} />
          <TitleText>{title}</TitleText>
          <Space height={24} />
          <Text style={{ textAlign: 'center' }}>{message}</Text>
          <NameText style={{ textAlign: 'center' }}>{name}</NameText>
          <Space height={24} />
          <Spacing>
            {onCancel && (
              <Button
                width="108px"
                height="42px"
                onClick={() => {
                  if (onCancel) {
                    onCancel();
                  }
                  setIsVisible(false);
                }}
                type={ButtonType.Default}
              >
                Hủy bỏ
              </Button>
            )}
            {onOke && (
              <Button
                onClick={() => {
                  if (onOke) {
                    onOke();
                  }
                  setIsVisible(false);
                }}
                type={ButtonType.Primary}
                height="42px"
                width="108px"
                loading={isLoading}
              >
                {okText || 'Xác nhận'}
              </Button>
            )}
          </Spacing>
        </WrapperContent>
      </Modal>
    </Wrapper>
  );
};

export default ConfirmModal;

const Wrapper = styled.div``;

const ModalStyles = css`
  .ant-modal-header {
    display: none !important;
  }
`;

const WrapperContent = styled.div`
  width: 100%;
  padding: 24px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const WrapperIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 160px;
  height: 160px;
  background: rgba(57, 181, 74, 0.2);
  color: ${props => props.theme.colors.primary.main};
  border-radius: 50%;
`;
