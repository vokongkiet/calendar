import React from 'react';
import { Column, Icon, Text } from 'farmhub-ui-core';
import { ImgSkeleton } from 'components';
import * as S from './styles';
import { ICONS } from 'utils/constants';
import { theme } from '../../../styled';

interface CategoryCardProps {
  selected: boolean;
  name: string;
  logo: string;
  onClick?: (e: any) => void;
}

const CategoryCard = ({
  selected,
  name,
  logo,
  onClick,
}: CategoryCardProps): JSX.Element => (
  <S.Wrapper selected={selected} onClick={onClick}>
    {logo ? <S.Image src={logo} /> : <ImgSkeleton width={84} height={54} />}
    <S.Content>
      <Column alignItems="center">
        <Text
          title={name}
          lineNumber={1}
          color="primary"
          weight="medium"
          styles={{ 'text-align': 'center', 'font-size': '13px' }}
        >
          {name}
        </Text>
      </Column>
    </S.Content>
    {selected && (
      <Icon
        svgProps={{
          icon: ICONS.CHECK,
          color: theme.colors.primary.main,
        }}
        styles={S.IconStyles}
      />
    )}
  </S.Wrapper>
);

export default CategoryCard;
