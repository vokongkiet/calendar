import * as React from 'react';

import { theme } from 'styled';
import { ICONS } from 'utils/constants';

import { Column, Icon, Space, Text } from 'farmhub-ui-core';

import * as S from './styles';

interface CertificateCardProps {
  selected: boolean;
  name: string;
  logo: string;
  onClick?: (e: any) => void;
}

const CertificateCard = ({
  selected,
  name,
  logo,
  onClick,
}: CertificateCardProps): JSX.Element => (
  <S.Wrapper selected={selected} onClick={onClick}>
    <Column alignItems="center">
      <Space height={12} />
      <S.Image src={logo} />
      <S.Content>
        <Column alignItems="center">
          <Text
            title={name}
            color="primary"
            weight="medium"
            lineNumber={2}
            styles={{ 'text-align': 'center' }}
          >
            {name}
          </Text>
        </Column>
      </S.Content>
    </Column>
    {selected && (
      <Icon
        svgProps={{
          icon: ICONS.CHECK,
          color: theme.colors.primary.main,
        }}
        styles={S.IconStyles}
      />
    )}
  </S.Wrapper>
);

export default CertificateCard;
