import React from 'react';
import styled from 'styled-components';
import { Select } from 'antd';
import {
  CaretDownOutlined,
  SortDescendingOutlined,
  SortAscendingOutlined,
} from 'farmhub-ui-core';
import { filterOption } from 'utils/function';

const { Option } = Select;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  width: 210px;
  height: 44px;
  border: 1px solid #d9d9d9;
  background: #fff;
  border-radius: 6px;
  padding-left: 12px;
`;

const CSelect = styled(Select)`
  width: 220px;
  height: 100%;

  .ant-select-selector {
    border: none !important;
    outline: none !important;
    box-shadow: none !important;
    .ant-select-selection-item {
      font-size: ${props => props.theme.font.size.xs};
      font-weight: ${props => props.theme.font.weight.medium};
      color: ${props => props.theme.colors.primary.main} !important;
      line-height: 42px !important;
    }
    &:active {
      outline: none !important;
      box-shadow: none !important;
      border: 0 !important;
    }
  }
`;

interface OrderTableProps {
  value?: any;
  list: Array<any>;
  onChange?: (e: any) => void;
}

const OrderTable = ({
  value,
  list,
  onChange = () => {},
}: OrderTableProps): JSX.Element => (
  <Wrapper>
    {value && value.value === 1 ? (
      <SortDescendingOutlined
        onClick={() =>
          onChange({
            ...value,
            value: 0,
          })
        }
        style={{ color: '#39B54A', fontSize: 18 }}
      />
    ) : (
      <SortAscendingOutlined
        onClick={() =>
          onChange({
            ...value,
            value: 1,
          })
        }
        style={{ color: 'red', fontSize: 18 }}
      />
    )}
    <CSelect
      showSearch
      filterOption={(input: string, option: any) => filterOption(input, option)}
      suffixIcon={<CaretDownOutlined />}
      size="large"
      value={value && value.name}
      onChange={e =>
        onChange({
          ...value,
          name: e,
        })
      }
    >
      {list.map(item => (
        <Option key={item.key} value={item.value}>
          {item.name}
        </Option>
      ))}
    </CSelect>
  </Wrapper>
);

export default OrderTable;
