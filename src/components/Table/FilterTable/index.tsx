import React from 'react';
import styled from 'styled-components';
import { Form } from 'antd';
import { OrderTable, SearchTable, CSelect } from 'components';
import { Row, Spacing } from 'farmhub-ui-core';

interface DataItem {
  name: string;
  list?: any;
  placeholder?: string;
}

interface FilterTableProps {
  name: string;
  searchName?: string;
  form: any;
  isDisplayFilter: boolean;
  data: DataItem[];
  ExtraAction?: React.ReactNode;
  ExtraFilter?: React.ReactNode;
  padding?: string;
  handleFilterFormChange?: (e: any) => void;
  borderBottom?: boolean;
  searchWidth?: number;
}

const FilterTable = ({
  name,
  searchName,
  isDisplayFilter,
  data,
  form,
  padding,
  ExtraAction,
  ExtraFilter,
  handleFilterFormChange,
  borderBottom = true,
  searchWidth,
}: FilterTableProps) => {
  return (
    <Form onValuesChange={handleFilterFormChange} form={form}>
      <WrapperFilter
        borderBottom={borderBottom}
        padding={padding}
        isDisplayFilter={isDisplayFilter}
      >
        <Spacing size={12}>
          {data.map((item: DataItem, index: number) => {
            if (item.name === 'order') {
              return (
                <Form.Item
                  key={index}
                  style={{ marginBottom: 0 }}
                  name={item.name}
                >
                  <OrderTable list={item.list} />
                </Form.Item>
              );
            } else {
              return (
                <Form.Item
                  key={index}
                  style={{ marginBottom: 0 }}
                  name={item.name}
                  initialValue={item.list.length > 0 ? item.list[0].value : ''}
                >
                  <CSelect
                    height="42px"
                    width="180px"
                    list={item.list}
                    placeholder={item.placeholder}
                  />
                </Form.Item>
              );
            }
          })}
        </Spacing>
        <Spacing size={12}>
          {ExtraAction}
          {searchName && (
            <Form.Item
              name={searchName || 'search'}
              style={{ marginBottom: 0 }}
            >
              <SearchTable
                width={searchWidth}
                placeholder={`Tìm theo tên ${name}`}
              />
            </Form.Item>
          )}
        </Spacing>
      </WrapperFilter>
      {ExtraFilter && (
        <WrapperFilter padding="18px 0px 0px" isDisplayFilter={isDisplayFilter}>
          <Row style={{ borderBottom: '1px solid #F1F1F1', paddingBottom: 18 }}>
            {ExtraFilter}
          </Row>
        </WrapperFilter>
      )}
    </Form>
  );
};

export default FilterTable;

const WrapperFilter = styled.div<{
  isDisplayFilter: boolean;
  padding?: string;
  borderBottom?: boolean;
}>`
  justify-content: space-between;
  display: ${p => (p.isDisplayFilter ? 'flex' : 'none')};
  width: 100%;
  height: auto;
  align-items: center;
  border-bottom: ${p => (p.borderBottom ? '1px solid #f1f1f1' : '')};
  padding: ${p => p.padding || '18px 0px'};
`;
