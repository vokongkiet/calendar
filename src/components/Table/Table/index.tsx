import * as S from './styles';

export enum SwitchSize {
  Default = 'default',
  Small = 'small',
}

const Table = ({
  switchSize = SwitchSize.Default,
  pagination,
  columns,
  dataSource,
  typeSelection,
  onRow = () => {},
  ...props
}: TableProps) => (
  <S.Table
    typeSelection={typeSelection}
    columns={columns}
    dataSource={dataSource}
    switchSize={switchSize}
    pagination={pagination}
    {...props}
  />
);

export default Table;

export interface TableProps {
  typeSelection?: string;
  loading?: boolean;
  switchSize?: SwitchSize;
  styles?: any;
  columns: any;
  dataSource: any;
  pagination?: any;
  bordered?: boolean;
  rowSelection?: any;
  components?: TableComponents;
  columnWidth?: string | number;
  expandable?: any;
  locale?: any;
  sticky?: boolean;
  scroll?: {
    scrollToFirstRowOnChange?: boolean;
    x?: string | number | true;
    y?: string | number;
  };
  onRow?: (e: any, index: number) => void;
  onChange?: (e: any) => void;
}

export interface TableComponents {
  table?: any;
  header?: {
    wrapper?: any;
    row?: any;
    cell?: any;
  };
  body?: {
    wrapper?: any;
    row?: any;
    cell?: any;
  };
}
