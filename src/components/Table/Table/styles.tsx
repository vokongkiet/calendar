import styled, { css } from 'styled-components';
import { Table as AntTable } from 'antd';

const TableStyles = css`
  .ant-checkbox-inner {
    background-color: #f1f1f1;
    display: inline-block;
    height: 22px;
    position: relative;
    width: 44px;
    border-radius: 999px;
    border: none;
  }

  .ant-checkbox-checked::after {
    border: none;
  }

  .ant-checkbox > .ant-checkbox-inner::after {
    width: 18px;
    height: 18px;
    background-color: #fff;
    border-radius: 50%;
    top: 2px;
    left: 2px;
    opacity: 1 !important;
    transform: none;
  }

  .ant-checkbox-inner::after {
    width: 18px;
    height: 18px;
    background-color: #fff;
  }

  .ant-checkbox-checked > .ant-checkbox-inner::after {
    transform: translate(22px, 0px);
    opacity: 1 !important;
  }

  .ant-checkbox-wrapper {
    .ant-checkbox-inner {
      width: 32px;
      background: #e1e1e1;
    }
  }

  .ant-table-selection {
    .ant-checkbox-wrapper-checked {
      .ant-checkbox-inner {
        width: 32px;
        background: ${props => props.theme.colors.primary.main};

        &::after {
          transform: translate(14px, 0px);
        }
      }
    }
  }

  .ant-table-row-selected {
    .ant-checkbox-wrapper-checked {
      .ant-checkbox-inner {
        width: 32px;
        background: ${props => props.theme.colors.primary.main};

        &::after {
          transform: translate(14px, 0px);
        }
      }
    }
  }
`;

const ChildTableStyles = css`
  .ant-checkbox-inner {
    background-color: #f1f1f1;
    display: inline-block;
    height: 18px;
    position: relative;
    width: 36px;
    border-radius: 999px;
    border: none;
  }

  .ant-checkbox-checked::after {
    border: none;
  }

  .ant-checkbox > .ant-checkbox-inner::after {
    width: 14px;
    height: 14px;
    background-color: #fff;
    border-radius: 50%;
    top: 2px;
    left: 2px;
    opacity: 1 !important;
    transform: none;
  }

  .ant-checkbox-inner::after {
    width: 14px;
    height: 14px;
    background-color: #fff;
  }

  .ant-checkbox-checked > .ant-checkbox-inner::after {
    transform: translate(19px, 0px);
    opacity: 1 !important;
  }

  .ant-checkbox-wrapper {
    .ant-checkbox-inner {
      width: 32px;
      background: #e1e1e1;
    }
  }

  .ant-table-selection {
    .ant-checkbox-wrapper-checked {
      .ant-checkbox-inner {
        width: 32px;
        background: ${props => props.theme.colors.primary.main};

        &::after {
          transform: translate(14px, 0px);
        }
      }
    }
  }

  .ant-table-row-selected {
    .ant-checkbox-wrapper-checked {
      .ant-checkbox-inner {
        width: 32px;
        background: ${props => props.theme.colors.primary.main};

        &::after {
          transform: translate(14px, 0px);
        }
      }
    }
  }
`;

export const Table = styled(AntTable)<{
  typeSelection?: string;
  switchSize: string;
  styles?: any;
}>`
  width: 100%;
  /* .ant-table-container table > thead > tr:first-child th:last-child {
    width: 120px;
  } */

  /* .ant-checkbox-checked > .ant-checkbox-inner {
    background-color: ${p => p.theme.colors.primary.main};
  } */

  .ant-table-cell {
    padding: 12px !important;
  }

  .ant-table-expand-icon-col {
    width: 24px;
  }

  .ant-table-row-expand-icon-cell {
    padding: 12px 0px !important;
    width: 24px !important;

    .ant-table-row-expand-icon,
    .ant-table-row-expand-icon-collapsed {
      width: 8px;
      height: 8px;
      border: none !important;
      background: white !important;
      outline: none;

      &::after {
        display: none;
      }

      &::before {
        width: 0;
        height: 0;
        background: #ffffff;
        border: none;
        outline: none;

        top: 0;
        left: 0;
        right: 0;

        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-top: 8px solid #acacac;
      }
    }

    .ant-table-row-expand-icon-collapsed::before {
      transform: rotate(-90deg);
    }
  }

  .ant-table-expanded-row {
    > td {
      padding: 12px 0px !important;
      background: #ffffff;

      &:hover {
        background: #ffffff;
      }
    }

    .ant-table {
      border-bottom: none;

      td {
        background: #ffffff;
      }
    }

    .ant-table-row-selected {
      > td {
        background: #ffffff;
      }
    }

    .ant-table-placeholder {
      .ant-table-cell {
        border-bottom: none;
      }
    }
  }

  .ant-table-row-selected {
    > td {
      background: #ffffff !important;
    }

    &:hover {
      > td {
        background: #fafafa !important;
      }
    }
  }

  .ant-table-empty {
    .ant-table-placeholder {
      .ant-table-cell {
        border-bottom: none;
      }
    }
  }

  ${p =>
    p?.typeSelection === 'checkbox'
      ? ''
      : p.switchSize === 'default' && TableStyles}
  ${p =>
    p?.typeSelection === 'checkbox'
      ? ''
      : p.switchSize === 'small' && ChildTableStyles}
      ${p => (p?.typeSelection === 'checkbox' ? '' : ChildTableStyles)}

  ${p => p.styles}
`;
