import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Input } from 'antd';

import { SearchOutlined } from 'farmhub-ui-core';
import { theme } from 'styled';

const Wrapper = styled(Input)<{
  background?: string;
  width: number;
  height?: number;
  fontSize?: string;
}>`
  height: ${p => p.height || 42}px;
  background: ${p => p.background} !important;
  width: ${p => p.width}px;
  .ant-input {
    font-size: ${p => p.theme.font.size[p.fontSize || 'sm']};
    background: ${p => p.background || '#fafafa'};

    &::placeholder {
      font-size: ${props => props.theme.font.size.xs} !important;
      color: #8c8c8c;
    }
  }
`;

interface SearchTableProps {
  value?: string;
  placeholder: string;
  onChange?: (e: any) => void;
  width?: number;
  maxWidth?: number;
  height?: number;
  background?: string;
  fontSize?: string;
}

const SearchTable = ({
  value = '',
  width,
  maxWidth,
  height,
  placeholder,
  onChange = () => {},
  background,
  fontSize,
}: SearchTableProps): JSX.Element => {
  const [isSearching, setIsSearching] = useState(false);
  const [valueChange, setValueChange] = useState('');

  useEffect(() => {
    setValueChange(value);
  }, [value]);

  return (
    <Wrapper
      width={width ? width : isSearching ? maxWidth || 240 : 42}
      height={height}
      onFocus={() => setIsSearching(true)}
      onBlur={() => setIsSearching(false)}
      background={background ? background : isSearching ? '#fff' : '#fff'}
      onPressEnter={(e: any) => {
        onChange(e);
        e.preventDefault();
      }}
      size="large"
      value={valueChange}
      onChange={e => setValueChange(e.target.value)}
      placeholder={width ? placeholder : isSearching ? placeholder : ''}
      prefix={
        <SearchOutlined
          style={{ fontSize: '18px', color: theme.colors.text.secondary }}
        />
      }
      fontSize={fontSize}
    />
  );
};

export default SearchTable;
