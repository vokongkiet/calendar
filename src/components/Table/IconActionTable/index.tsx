import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { IconWrapper } from 'components';
import { Space } from 'farmhub-ui-core';
import { ExtendedAction } from 'containers/WorkSpace/components';
import { MoreIcon } from 'components/Icon';

type ActionTable = {
  action?: any;
  icon: any;
  tooltip?: string;
  popover?: React.ReactNode;
  hoverColorIcon?: string;
};

interface IconActionTableProps {
  data: ActionTable[];
  extendedAction?: React.ReactNode;
  noteAction?: React.ReactNode;
}

const IconActionTable = ({
  data,
  extendedAction,
  noteAction,
}: IconActionTableProps): JSX.Element => {
  const [showActionList, setShowActionList] = useState<any[]>([]);
  const [hiddenActionList, setHiddenActionList] = useState<any[]>([]);

  useEffect(() => {
    const initData = () => {
      // if (data.length === 8) {
      //   setShowActionList(data);
      //   setHiddenActionList([]);
      //   return;
      // }

      const countShowItem = noteAction ? 1 : 2;

      const newShowActionList = data.slice(0, countShowItem);
      const newHiddenActionList = data
        .slice(countShowItem)
        .map((item: any, index: number) => ({
          key: index,
          name: item.tooltip,
          action: item.action,
          icon: item.icon,
          hoverColorIcon: item.hoverColorIcon,
        }));
      setShowActionList(newShowActionList);
      setHiddenActionList(newHiddenActionList);
    };

    initData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <Wrapper width={'100%'}>
      {noteAction && (
        <>
          {noteAction} <Space width={8} />
        </>
      )}
      {showActionList.map((item: any, index: number) => {
        return (
          <IconWrapper
            key={index}
            styles={{
              'margin-right': '8px',
            }}
            onClick={item.action}
            hoverBackground="rgba(57, 181, 74, 0.2)"
            size={36}
            background="#F1F1F1"
            icon={item.icon}
            tooltip={item.tooltip}
            popover={item.popover}
            hoverColorIcon={item.hoverColorIcon}
          />
        );
      })}
      {hiddenActionList.length !== 0 && (
        <ExtendedAction
          component={
            <IconWrapper
              onClick={(e: any) => {
                e.stopPropagation();
              }}
              hoverBackground="rgba(57, 181, 74, 0.2)"
              size={36}
              background="#F1F1F1"
              icon={<MoreIcon width={18} height={18} />}
              tooltip="Thêm chức năng"
            />
          }
          data={hiddenActionList}
        />
      )}

      {extendedAction}
    </Wrapper>
  );
};

export default IconActionTable;

const Wrapper = styled.div<{ width: string }>`
  width: ${p => p.width};
  display: flex;
  justify-content: flex-end;
  flex-shrink: 0;
`;
