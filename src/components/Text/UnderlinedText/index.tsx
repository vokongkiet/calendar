import styled, { css } from 'styled-components';
import { TextProps } from 'farmhub-ui-core';

const UnderlinedText = styled.span<TextProps>`
  word-break: break-word;
  color: ${p => p.theme.colors.text[p.color || 'link']};
  line-height: ${p => p.lineHeight || '22px'};
  font-size: ${p => p.theme.font.size[p.size || 'xs']};
  font-weight: ${p => p.theme.font.weight[p.weight || 'regular']} !important;
  ${p =>
    p.textTransform &&
    css`
      text-transform: ${p.textTransform};
    `};
  ${p =>
    p.capitalizeFirstLetter &&
    css`
      &::first-letter {
        text-transform: capitalize !important;
      }
    `};
  ${p => p.styles}

  text-decoration-line: underline;
  white-space: pre-wrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-line-clamp: ${props => props.lineNumber};
  -webkit-box-orient: vertical;
  display: -webkit-box;
  cursor: pointer;
`;

export default UnderlinedText;
