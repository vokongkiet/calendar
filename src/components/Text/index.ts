export { default as NameText } from './NameText';
export { default as PriceText } from './PriceText';
export { default as TinyText } from './TinyText';
export { default as TitleText } from './TitleText';
export { default as UnderlinedText } from './UnderlinedText';
