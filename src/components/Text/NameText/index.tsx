import styled, { css } from 'styled-components';
import { TextProps } from 'farmhub-ui-core';

const NameText = styled.span<NameTextProps>`
  word-break: break-word;
  color: ${p => p.theme.colors.text[p.color || 'primary']} !important;
  line-height: ${p => p.lineHeight || '26px'};
  font-size: ${p => p.theme.font.size[p.size ? p.size : 'xs']} !important;
  font-weight: ${p =>
    p.theme.font.weight[p.weight ? p.weight : 'bold']} !important;
  ${p =>
    p.textTransform &&
    css`
      text-transform: ${p.textTransform};
    `};
  ${p =>
    p.capitalizeFirstLetter &&
    css`
      &::first-letter {
        text-transform: capitalize !important;
      }
    `};
  ${p => p.styles}

  white-space: pre-wrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-line-clamp: ${props => props.lineNumber};
  -webkit-box-orient: vertical;
  display: -webkit-box;
  /* text-align: left !important; */
`;

export default NameText;

type NameTextProps = Pick<TextProps, Exclude<keyof TextProps, ''>>;
