import * as React from 'react';

import Section, { SectionProps } from '../BaseSection';

import * as S from './styles';

const ScrollSection = ({
  children,
  sectionProps,
  scrollProps,
}: ScrollSectionProps): JSX.Element => (
  <Section {...sectionProps}>
    <S.ScrollBody {...scrollProps}>{children}</S.ScrollBody>
  </Section>
);

export default ScrollSection;

export interface ScrollProps {
  height: string;
  padding?: string;
}

export interface ScrollSectionProps {
  sectionProps?: Pick<SectionProps, Exclude<keyof SectionProps, 'children'>>;
  scrollProps: ScrollProps;
  children: React.ReactNode;
}
