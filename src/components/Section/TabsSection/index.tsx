import * as React from 'react';

import { Tabs, TabsProps } from 'farmhub-ui-core';

import Section, { SectionProps } from '../BaseSection';

const TabsSection = ({
  children,
  sectionProps,
  tabsProps,
}: TabsSectionProps): JSX.Element => (
  <Section {...sectionProps}>
    <Tabs {...tabsProps}>{children}</Tabs>
  </Section>
);

export default TabsSection;

export interface TabsSectionProps {
  sectionProps?: Pick<SectionProps, Exclude<keyof SectionProps, 'children'>>;
  tabsProps: Pick<TabsProps, Exclude<keyof TabsProps, 'children'>>;
  children: React.ReactNode;
}
