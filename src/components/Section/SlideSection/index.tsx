import * as React from 'react';

import { Row } from 'farmhub-ui-core';

import { SlideShow, SlideShowProps } from 'components';
import { DEFAULT_WIDTH_AS_NUMBER } from 'utils/constants';

import Section, { SectionProps } from '../BaseSection';

const SlideSection = ({
  children,
  sectionProps,
  slideShowProps,
}: SlideSectionProps): JSX.Element => {
  const refDiv = React.useRef<HTMLDivElement>(null);
  const [width, setWidth] = React.useState<number>(DEFAULT_WIDTH_AS_NUMBER);

  React.useEffect(() => {
    setWidth(
      refDiv.current ? refDiv.current.offsetWidth : DEFAULT_WIDTH_AS_NUMBER,
    );
  }, []);

  return (
    <Section {...sectionProps}>
      <Row ref={refDiv} />
      <SlideShow width={width} {...slideShowProps}>
        <Row>{children}</Row>
      </SlideShow>
    </Section>
  );
};

export default SlideSection;

export interface SlideSectionProps {
  sectionProps?: Pick<SectionProps, Exclude<keyof SectionProps, 'children'>>;
  slideShowProps: Pick<
    SlideShowProps,
    Exclude<keyof SlideShowProps, 'children'>
  >;
  children: React.ReactNode;
  name?: string;
}
