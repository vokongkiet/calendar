export { default as SlideShowWithMainImageSkeleton } from './Skeletons/SlideShowWithMainImage';
export { default as ImageSlideShow } from './ImageSlideShow';
export { default as SlideShow } from './SlideShow';

export * from './ImageSlideShow';
export * from './SlideShow';
