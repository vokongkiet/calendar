import googleService from './services/google';
import { serialize } from '../utils/crud';
// #endregion Local Imports

export const GoogleApi = {
  getLocationInfo: async (option: any) => {
    let url = '/geocode/json';
    const query = serialize(option);
    url += query;

    try {
      const response = await googleService.get(url);
      return response;
    } catch (error) {
      throw error;
    }
  },
};
