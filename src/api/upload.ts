import uploadService from './services/upload';

export const UploadApi = {
  uploadSingleFile: async (id: string, type: string, file: any) => {
    const formData = new FormData();
    if (file.length > 0) {
      file.forEach(item => formData.append('files', item));
    } else {
      formData.append('files', file);
    }
    try {
      const response = await uploadService.post(
        `/cdn?id=${id}&type=${type}`,
        formData,
      );
      return response;
    } catch (error) {
      throw error;
    }
  },
};
