import axios from 'axios';
import queryString from 'query-string';
import { env } from 'env';

// Please have a look at here `https://github.com/axios/axios#request-config` for the full list of configs
const { CancelToken } = axios;
export const sourceCancel = CancelToken.source();

const baseURLEntity = `${env.hubtech.entityService}/api/v1`;

// entity service
const entityService = axios.create({
  baseURL: baseURLEntity,
  cancelToken: sourceCancel.token,
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/json',
  },
  paramsSerializer: params => queryString.stringify(params),
});

entityService.interceptors.response.use(
  response => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  error => {
    throw error;
  },
);

export const getTokenBearer = () =>
  entityService.defaults.headers.common.Authorization;

export const updateTokenBearer = token => {
  entityService.defaults.headers.common.Authorization = token;
};

export const updateTokenID = token => {
  entityService.defaults.headers.common['token-id'] = token;
};

export const updateCurrentEntity = (entityID, type) => {
  entityService.defaults.headers.common['entity-id'] = entityID;
  entityService.defaults.headers.common['type'] = type;
};

export const removeCurrentEntity = () => {
  delete entityService.defaults.headers.common['entity-id'];
  delete entityService.defaults.headers.common['type'];
};

export default entityService;
