import axios from 'axios';
import queryString from 'query-string';
import { env } from 'env';

// Please have a look at here `https://github.com/axios/axios#request-config` for the full list of configs
const { CancelToken } = axios;
export const sourceCancel = CancelToken.source();

const baseURLGoogle = `${env.google.googleMapService}/api`;

// google service
const googleService = axios.create({
  baseURL: baseURLGoogle,
  cancelToken: sourceCancel.token,
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/json',
  },
  paramsSerializer: params => queryString.stringify(params),
});

googleService.interceptors.response.use(
  response => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  error => {
    // Handle errors
    throw error;
  },
);

export const getTokenBearer = () =>
  googleService.defaults.headers.common.Authorization;

export const updateTokenBearer = (token: string) => {
  googleService.defaults.headers.common.Authorization = token;
};

export default googleService;
