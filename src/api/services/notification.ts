import axios from 'axios';
import queryString from 'query-string';
import { env } from 'env';

// Please have a look at here `https://github.com/axios/axios#request-config` for the full list of configs
const { CancelToken } = axios;
export const sourceCancel = CancelToken.source();

const baseURLNotification = `${env.hubtech.notificationService}/api/v1`;

// notification service
const notificationService = axios.create({
  baseURL: baseURLNotification,
  withCredentials: true,
  cancelToken: sourceCancel.token,
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/json',
  },
  paramsSerializer: params => queryString.stringify(params),
});

notificationService.interceptors.response.use(
  response => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  error => {
    // Handle errors
    throw error;
  },
);

export const getTokenBearer = () =>
  notificationService.defaults.headers.common.Authorization;

export const updateTokenBearer = (token: string) => {
  notificationService.defaults.headers.common.Authorization = token;
};

export default notificationService;
