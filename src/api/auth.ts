import identityService from 'api/services/identity';
import { Credentials } from 'containers/App/Auth/authSlice';

export const AuthApi = {
  login: async (credentials: Credentials) => {
    try {
      const response = await identityService.post(
        '/users/login-with-password',
        credentials,
      );
      return response;
    } catch (error) {
      throw error;
    }
  },

  refreshAccessToken: async () => {
    try {
      const response = await identityService.post('/users/refresh-token');
      return response;
    } catch (error) {
      throw error;
    }
  },

  logout: async () => {
    try {
      const response = await identityService.post(`/users/logout`);
      return response;
    } catch (error) {
      throw error;
    }
  },

  logoutAll: async () => {
    try {
      const response = await identityService.post(`/user/logout-all`);
      return response;
    } catch (error) {
      throw error;
    }
  },
};
