import entityService from './services/entity';
import { serialize } from '../utils/crud';
// #endregion Local Imports

export const EntityApi = {
  find: async (option: any = {}) => {
    let url = '/entities';
    const query = serialize(option);
    url += query;

    try {
      const response = await entityService.get(url);

      return response;
    } catch (error) {
      throw error;
    }
  },

  findWithAuth: async (option: FindWithAuthOption = {}) => {
    let url = '/entities/auth';
    const query = serialize(option);
    url += query;

    try {
      const response = await entityService.get(url);

      return response;
    } catch (error) {
      throw error;
    }
  },

  create: async (body: any) => {
    try {
      const response = await entityService.post(`/entities/entity`, body);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  createAnonymous: async (body: any) => {
    try {
      const response = await entityService.post(
        `/entities/anonymous-entity`,
        body,
      );
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  findOne: async (id: string, option: any = {}) => {
    let url = `/entities/${id}`;
    const query = serialize(option);
    url += query;

    try {
      const response = await entityService.get(url);

      return response;
    } catch (error) {
      throw error;
    }
  },

  update: async (id: string, body: any = {}) => {
    const url = `/entities/${id}`;

    try {
      const response = await entityService.patch(url, body);
      return response;
    } catch (error) {
      throw error;
    }
  },

  getLocationID: async (provinceCode: string, districtCode: string) => {
    try {
      const response = await entityService.get(
        `/locations?where={"provinceCode":"${provinceCode}","districtCode":"${districtCode}"}`,
      );
      return response.data;
    } catch (error) {
      throw error;
    }
  },
};

export interface FindWithAuthOption {
  page?: number;
  take?: number;
  relations?: string[];
  select?: string[];
  order?: any;
}
