/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import authReducer from 'containers/App/Auth/authSlice';
import entityReducer from 'containers/App/Account/entitySlice';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
const rootReducer = combineReducers({
  auth: authReducer,
  entity: entityReducer,
});

export default rootReducer;
