import { createGlobalStyle } from 'styled-components';
import { theme } from './theme/theme';

export const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5;
    font-weight: ${p => theme.font.weight.regular};
  }

  svg { vertical-align: baseline; }

  body {
    font-family: 'Open Sans', sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', sans-serif;
  }

  #app {
    background-color: #FFFFFF;
    min-height: 100%;
    min-width: 100%;
  }

  .ant-table {
    .ant-table-thead {
      .ant-table-cell {
        background: #FFF;
        color: #262626;
        font-weight: ${p => theme.font.weight.medium};

        &:before {
          display: none;
        }
      }
    }
  }

  /* Let's get this party started */
::-webkit-scrollbar {
  width: 8px;
  height: 8px;
  cursor: pointer;
}

/* Track */
::-webkit-scrollbar-track {
  background: #FAFAFA; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  border-radius: 999px;
  background: #CCCCCC; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #8C8C8C;
  width: 12px;
  height: 12px;
}

  .ant-tabs {
    .ant-tabs-tab {
      margin: 0px;
      padding: 12px 36px;
    }
    .ant-tabs-tab-btn {
      font-weight: ${p => theme.font.weight.regular};
      color: #595959;
    }

    .ant-tabs-tab.ant-tabs-tab-active {
      .ant-tabs-tab-btn {
        font-weight: ${p => theme.font.weight.bold};
      }
    }
  }

  .ant-modal-mask {
    background-color: rgba(0, 0, 0, 0.25) !important;
  }

  .ant-select-item.ant-select-item-option{
    padding: 12px;
    color: #595959;
    font-weight: ${p => theme.font.weight.regular};
  }

  .ant-select-item.ant-select-item-option.ant-select-item-option-active {
    color: #39B54A;
  }

  .ant-image-preview-wrap {
    background-color: rgba(0, 0, 0, 0.8);
  }
`;
