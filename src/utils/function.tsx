/* eslint-disable */
import React from 'react';

import { Skeleton } from 'antd';
import location from 'commons/location.json';
import {
  BoxColorIcon,
  FarmerColorIcon,
  IncognitoColorIcon,
  PeopleLinkColorIcon,
  ShopColorIcon,
  TruckColorIcon,
  VolunteerColorIcon,
} from 'components/Icon';
import {
  CheckCircleOutlined,
  ClockCircleOutlined,
  CloseCircleOutlined,
  EditOutlined,
  Notification,
  Spacing,
  SyncOutlined,
  Text,
  CheckSquareOutlined
} from 'farmhub-ui-core';
import moment from 'moment';

import { unwrapResult } from '@reduxjs/toolkit';
import { theme } from 'styled';
import { MENU_TITLE_ORDER_BY_STATUS } from './constants';

export const formatMoney = number => {
  if (!number || number === 0) return 0;
  // if (!number) return '';
  return (
    number &&
    number
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      .replace(/-/g, '- ')
  );
};

export function formatAppearPhoneNumber(phoneNumber: string) {
  const cleaned = ('' + phoneNumber).replace(/\D/g, '').replace('84', '0');
  const match = cleaned.match(
    /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/,
  );

  if (match) {
    return (
      '(' +
      match[1] +
      ') ' +
      [match[3].slice(0, 3), ' ', match[3].slice(4)].join('')
    );
  }
  return phoneNumber;
}

export function removeVietnameseTones(str: string) {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  // Some system encode vietnamese combining accent as individual utf-8 characters
  // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
  // Remove extra spaces
  // Bỏ các khoảng trắng liền nhau
  str = str.replace(/ + /g, ' ');
  str = str.trim();
  // Remove punctuations
  // Bỏ dấu câu, kí tự đặc biệt
  str = str.replace(
    /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
    ' ',
  );
  return str;
}

export const detechTime = createdAt => {
  const diffMinute = moment().diff(createdAt, 'm');
  if (diffMinute === 0) {
    return 'Mới gần đây';
  }
  if (diffMinute < 60) {
    return `${diffMinute} phút trước`;
  }
  if (diffMinute < 60 * 24) {
    return `${Math.floor(diffMinute / 60) + 1} giờ trước`;
  }
  if (diffMinute < 60 * 24 * 30) {
    return `${Math.floor(diffMinute / (60 * 24)) + 1} ngày trước`;
  }
  if (diffMinute < 60 * 24 * 30 * 12) {
    return `${Math.floor(diffMinute / (60 * 24 * 30)) + 1} tháng trước`;
  }
  return `${Math.floor(diffMinute / (60 * 24 * 30 * 12)) + 1} năm trước`;
};

export const renderObjectType = type => {
  switch (type) {
    case 0:
      return <Text>Cây</Text>;
    case 1:
      return <Text>Luống</Text>;
    case 2:
      return <Text>Toàn bộ vùng</Text>;
    case 3:
      return <Text>Trang trại</Text>;
    case 4:
      return <Text>Nhà kính</Text>;
    case 5:
      return <Text>Khác</Text>;
    default:
      return '';
  }
};

export const isVietnamesePhoneNumber = number =>
  /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/.test(number) //eslint-disable-line

export const formatPhoneNumber = number => {
  if (!number) {
    return undefined;
  };

  switch (number[0]) {
    case '0':
      return number.replace('0', '+84');
    case '8':
      return `+${number}`;
    default:
      return number;
  }
};


export const validateEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //eslint-disable-line
  return re.test(String(email).toLowerCase());
};

export const validateURL = str => {
  var pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
    'i',
  ); // fragment locator
  return !!pattern.test(str);
};

export const getQueryString = ({
  search,
  field = '',
  fields = [] as string[],
  defaultValue = '',
  whiteList = [] as string[],
}): any => {
  const value: string = new URLSearchParams(search).get(field) || '';

  let fieldResults = {};
  if (fields.length > 0) {
    fields.forEach(key => {
      fieldResults[key] = new URLSearchParams(search).get(key) || '';
    });

    return fieldResults;
  }

  if (whiteList.length === 0) {
    return value || defaultValue;
  }
  if (whiteList.includes(value)) {
    return value;
  }

  return defaultValue;
};

export const truncate = (str: string, n: number) => {
  return str.length > n ? str.substr(0, n - 1) + '...' : str;
};

export const truncateWords = (text: string, wordNumber: number) => {
  const wordList = text?.split(' ');
  return `${wordList?.slice(0, wordNumber).join(' ')}...`;
};

export const parseUtils = (obj, key, type) => {
  if (obj[key] || obj[key] === 0) {
    if (key === 'avatar') {
      obj.avatarThumbnail = obj[key] && obj[key][0]?.urlThumbnail[0];
    }

    if (key === 'banner') {
      obj.bannerThumbnail = obj[key] && obj[key][0]?.urlThumbnail[0];
    }

    obj[key] = parseType(type, obj[key]);
  }
};

const parseType = (type, value) => {
  switch (type) {
    case 'string': {
      if (value === 0) {
        return '0';
      } else {
        return value.toString();
      }
    }

    case 'images':
      return (
        value &&
        value.map(data => {
          if (!data?.isVideo) {
            return {
              type: 0,
              url: data.url,
              urlThumbnail: data.urlThumbnail,
            };
          } else {
            return {
              type: 1,
              url: data[0].url[0],
              urlThumbnail: data[0].urlThumbnail[0],
            };
          }
        })
      );

    case 'video':
      return (
        value && {
          type: 1,
          url: value[0]?.url[0],
          urlThumbnail: value[0]?.urlThumbnail[0],
        }
      );

    case 'url':
      return value && value.map(data => data.url);

    case 'image':
      return value && value[0]?.url[0];

    default:
      return value && parseInt(value, 0);
  }
};

export const parseGalleryImage = async (url: string) => {
  const img = new Image();
  img.src = url;
  await img.decode();
  return {
    src: url,
    width: img.width,
    height: img.height,
  };
};

export const removeEmptyElement = (body: any) => {
  Object.keys(body).forEach(e => {
    if (body[e] === undefined || body[e] === null) {
      delete body[e];
    }
  });
};

export const renderTableSkeleton = columns => {
  const skeleton = columns.map((column, index) => {
    let renderContent;

    switch (column.type) {
      case 'image':
        renderContent = (
          <Spacing>
            <Skeleton.Image style={{ width: 60, height: 60 }} />
            <Skeleton.Input
              size="small"
              style={{ width: column.width }}
              active
            />
          </Spacing>
        );
        break;
      case 'button':
        renderContent = <Skeleton.Button active size="small" />;
        break;
      case 'banner':
        renderContent = (
          <Skeleton.Image style={{ width: column.width, height: 110 }} />
        );
        break;
      default:
        renderContent = (
          <Skeleton.Input size="small" style={{ width: column.width }} active />
        );
    }

    const result = {
      title: column.title,
      key: index.toString(),
      render: () => renderContent,
    };

    return column.width
      ? {
        ...result,
        width:
          column.type === 'image' && typeof column.width === 'number'
            ? column.width + 60
            : column.width,
      }
      : result;
  });

  return skeleton;
};

export const renderColorTag = (name: string) => {
  switch (name) {
    case 'Chiến dịch':
      return 'gold';
    case 'Mới':
      return 'green';
    case 'Free ship':
      return 'blue';
    case 'Organic':
      return 'red';
    case 'Free ship > 1000k':
      return 'cyan';
    case 'Hỗ trợ ship 30k':
      return 'geekblue';
    case 'Hữu cơ':
      return 'lime';
    case 'An toàn':
      return 'orange';
    default:
      return 'green';
  }
};

export const renderEmployeeRole = (role: number) => {
  switch (role) {
    case 0:
      return 'Nhân viên';
    case 1:
      return 'Quản lý';
    default:
      return 'Trống';
  }
};

export const getEntityInfo = (type: number) => {
  switch (type) {
    case 1:
      return {
        name: 'Nông trại',
        icon: <FarmerColorIcon width={18} height={18} />,
      };
    case 2:
      return {
        name: 'Cửa hàng',
        icon: <ShopColorIcon width={18} height={18} />,
      };
    case 3:
      return {
        name: 'Cộng tác viên',
        icon: <PeopleLinkColorIcon width={18} height={18} />,
      };
    case 4:
      return {
        name: 'Kho chứa',
        icon: <BoxColorIcon width={18} height={18} />,
      };
    case 5:
      return {
        name: 'Vận chuyển',
        icon: <TruckColorIcon width={18} height={18} />,
      };
    case 6:
      return {
        name: 'Thiện nguyện',
        icon: <VolunteerColorIcon width={18} height={18} />,
      };
    case 7:
      return {
        name: 'Ẩn danh',
        icon: <IncognitoColorIcon width={18} height={18} />,
      };
    default:
      return {};
  }
};

export const genEntityName = (type: number) => {
  switch (type) {
    case 0:
      return 'Người dùng';
    case 1:
      return 'Nông trại';
    case 2:
      return 'Đại lý';
    case 3:
      return 'Cộng tác viên';
    case 4:
      return 'Kho chứa';
    case 5:
      return 'Vận chuyển';
    case 6:
      return 'Thiện nguyện';
    case 7:
      return 'Ẩn danh';
    default: return '';
  }
}

export const reportQuantity = (reportInfo: any) => {
  const { reportType, relativeQuantity, absoluteQuantity } = reportInfo;
  switch (reportType) {
    case 0:
      switch (relativeQuantity) {
        case 0:
          return 'Hết hàng';
        case 1:
          return 'Có ít';
        case 2:
          return 'Có vừa';
        case 3:
          return 'Có nhiều';
        default:
          return 0;
      }
    case 1:
      return formatMoney(absoluteQuantity);
    default:
      return 0;
  }
};

export const getRelativeQuantityColor = (reportInfo: any) => {
  const { reportType, relativeQuantity } = reportInfo;
  if (reportType === 0) {
    switch (relativeQuantity) {
      case 0:
        return theme.colors.text.red;
      case 1:
        return theme.colors.text.price;
      case 2:
        return theme.colors.text.orange;
      case 3:
        return theme.colors.text.green;
      default:
        return '#262626';
    }
  }
  return '#262626';
};

export const getOrderStatus = (status: number) => {
  switch (status) {
    case 0:
      return {
        name: MENU_TITLE_ORDER_BY_STATUS.PENDING,
        color: 'geekblue',
        icon: <ClockCircleOutlined />,
      };
    case 1:
      return { name: MENU_TITLE_ORDER_BY_STATUS.PROCESSING, color: 'warning', icon: <SyncOutlined /> };
    case 2:
      return { name: MENU_TITLE_ORDER_BY_STATUS.DELIVERY, color: 'processing', icon: <SyncOutlined /> };
    case 3:
      return {
        name: MENU_TITLE_ORDER_BY_STATUS.CONFIRM,
        color: 'cyan',
        icon: <SyncOutlined />,
      };
    case 4:
      return { name: MENU_TITLE_ORDER_BY_STATUS.REJECT, color: 'error', icon: <CloseCircleOutlined /> };
    case 5:
      return { name: MENU_TITLE_ORDER_BY_STATUS.DRAF, color: 'purple', icon: <EditOutlined /> };
    case 6:
      return {
        name: MENU_TITLE_ORDER_BY_STATUS.COMPLETE,
        color: 'success',
        icon: <CheckCircleOutlined />,
      };
    case 7:
      return {
        name: MENU_TITLE_ORDER_BY_STATUS.PRE_PROCESSING,
        color: 'default',
        icon: <CheckCircleOutlined />,
      };
      case 8:
        return {
          name: MENU_TITLE_ORDER_BY_STATUS.SUPPLIER_CONFIRM,
          color: 'default',
          icon: <CheckSquareOutlined />, // TODO: update icon
        };
    default:
      return { name: 'Trống', color: 'default', icon: <ClockCircleOutlined /> };
  }
};

export const getCouponStatus = (createdTime: string, endedTime: string) => {
  let status = 0
  if (moment() >= moment(createdTime) && moment() <= moment(endedTime)) {
    status = 1
  }
  else if (moment() >= moment(endedTime)) {
    status = 2
  }

  switch (status) {
    case 0:
      return {
        name: 'Sắp diễn ra',
        color: 'warning',
        icon: <ClockCircleOutlined />,
      };
    case 1:
      return { name: 'Đang hoạt động', color: 'success', icon: <SyncOutlined /> };
    case 2:
      return { name: 'Đã kết thúc', color: 'default', icon: <CheckCircleOutlined /> };
    default:
      return { name: '', color: 'default', icon: <ClockCircleOutlined /> };
  }
}

export const getCobuyStatus = (status: number) => {
  switch (status) {
    case 0:
      return {
        name: 'Đang hoạt động',
        color: 'success',
        icon: <ClockCircleOutlined />,
      };
    case 1:
      return { name: 'Dừng hoạt động', color: 'warning', icon: <SyncOutlined /> };
    case 2:
      return { name: 'Đang hoạt động', color: 'success', icon: <CheckCircleOutlined /> };
    case 3:
      return {
        name: 'Đã hết hạn',
        color: 'error',
        icon: <CheckCircleOutlined />,
      };
    default:
      return { name: '', color: 'default', icon: <ClockCircleOutlined /> };
  }
};

export const getOrderType = (type: number) => {
  switch (type) {
    case typeOrderValue.wholeSale:
      return { name: 'Đơn sỉ', color: 'cyan' };
    case typeOrderValue.retail:
      return { name: 'Đơn lẻ', color: 'blue' };
    case typeOrderValue.coBuy:
      return { name: 'Đơn mua chung', color: 'magenta' };
    default:
      return { name: '', color: 'default' };
  }
};

export const typeOrderValue = {
  wholeSale: 0,
  retail: 1,
  coBuy: 2,
};

export const statusOrder = {
  0: 'Chờ xử lý',
  1: 'Đang xử lý',
  2: 'Giao hàng',
  3: 'Hoàn thành',
  4: 'Từ chối',
  5: 'Bản nháp',
};

export const confirmOrderStatus = {
  1: 'Xử lý đơn hàng',
  2: 'Giao hàng',
  4: 'Từ chối đơn hàng',
  5: 'Đưa vào đơn hàng nháp',
};

export const activityType = (type: number) => {
  switch (type) {
    case 0:
      return 'đã mua đơn hàng';
    case 10:
      return 'gửi lời mời liên kết';
    case 11:
      return 'chấp nhận lời mời liên kết';
    case 20:
      return 'thực hiện chiến dịch';
    case 30:
      return 'cập nhật trạng thái đơn hàng';
    case 31:
      return 'trộn đơn hàng';
    case 32:
      return 'chuyển tiếp đơn hàng';
    case 33:
      return 'cập nhật thực giao và chuyển trạng thái giao hàng';
    case 34:
      return 'cập nhật thực nhận và chuyển trạng thái hoàn thành';
    case 40:
      return 'yêu thích sản phẩm';
    case 41:
      return 'đánh giá sản phẩm';
    case 42:
      return 'đánh giá';
    case 43:
      return 'đánh giá đối tượng nuôi trồng';
    case 44:
      return 'mời bạn tham gia nhóm mua chung';
    case 45:
      return 'mời bạn tham gia chiến dịch';
    default:
      return 'thông báo';
  }
};

export const genNameListActivityType = (activityType: number) => {
  switch (activityType) {
    case 10 || 11:
      return 'linkedList';
    case 20:
      return 'systemList';
    case 30 || 31 || 32 || 33 || 34:
      return 'orderList';
    case 0:
      return 'requestList';
    default:
      return '';
  }
};

export const getDistrict: any = (
  provinceCode: string,
  districtCode: string,
) => {
  const province = location.find(
    (item: any) => item.province_code === provinceCode,
  );
  if (province) {
    return province.districtInfo.find(
      (item: any) => item.district_code === districtCode,
    );
  }
};

export const getProvince = (provinceCode: string) => {
  const province = location.find(
    (item: any) => item.province_code === provinceCode,
  );
  if (province) {
    return {
      province_code: province.province_code,
      province_name: province.province_name,
    };
  }
  return { province_code: '', province_name: '' };
};

export const getProvinceList = () =>
  location.map((item: any) => ({
    province_code: item.province_code,
    province_name: item.province_name,
  }));

export const getDistrictList = (provinceCode: string | undefined) => {
  const province = location.find(
    (item: any) => item.province_code === provinceCode,
  );
  if (province) {
    return province.districtInfo;
  }
  return [];
};

export const renderTextConfirm = (status: number) => {
  switch (status) {
    case 0:
      return 'Chờ xử lý đơn hàng';
    case 1:
      return 'Xử lý đơn hàng';
    case 2:
      return 'Giao hàng';
    case 3:
      return 'Thực nhận đơn hàng';
    case 4:
      return 'Từ chối đơn hàng';
    case 5:
      return 'Đưa vào đơn hàng nháp';
    case 6:
      return 'Hoàn thành đơn hàng';
    default:
      return '';
  }
};

export const filterOption = (input: string, option: any) => {
  if (
    removeVietnameseTones(
      typeof option.children === 'string'
        ? option.children.toLocaleLowerCase()
        : option.label.toLocaleLowerCase(),
    ).includes(removeVietnameseTones(input.toLocaleLowerCase()))
  ) {
    return true;
  }
  return false;
};

export const getDummyArray = (size: number) =>
  Array(size)
    .fill('0')
    .map((item, index) => ({ key: index }));

export const genQRCodeNumber = (order: number) => {
  switch (true) {
    case order < 10:
      return '00' + order;
    case order < 100:
      return '0' + order;
    case order < 1000:
      return order;
    case order < 10000:
      return '00' + order;
    case order < 100000:
      return '0' + order;
    case order < 1000000:
      return order;
    default:
      return order;
  }
};

export const convertToSlug = (text: string) => {
  const a =
    'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;';
  const b =
    'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------';
  const p = new RegExp(a.split('').join('|'), 'g');
  const formatText = text
    .toString()
    .toLowerCase()
    .replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a')
    .replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e')
    .replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i')
    .replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o')
    .replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u')
    .replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y')
    .replace(/đ/gi, 'd')
    .replace(/\s+/g, '-')
    .replace(p, c => b.charAt(a.indexOf(c)))
    .replace(/&/g, '-and-')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');

  const uniqueNumber = moment().unix();
  return `${formatText}-${uniqueNumber}`;
};

export const getFullAddress = (
  address: string | undefined,
  district: string | undefined,
  province: string | undefined
) => {
  let fullAddress = '';
  if (address) {
    fullAddress += address;
  }
  if (district) {
    fullAddress += `, ${district}`;
  }
  if (province) {
    fullAddress += `, ${province}`;
  }
  return fullAddress || 'Trống';
};

export const getPaymentMethod = (value: number) => {
  switch (value) {
    case 0:
      return 'Thanh toán khi nhận hàng';
    case 1:
      return 'Ví điện tử';
    case 2:
      return 'Internet Banking';
    case 3:
      return 'Thẻ tín dụng/Thẻ ghi nợ';
    default:
      return '';
  }
};

export const getDeliveryMethod = (value: number) => {
  switch (value) {
    case 0:
      return 'Giao hàng Farmhub Fast';
    case 1:
      return 'Nhận tại cửa hàng';
    default:
      return '';
  }
};

export const parseQueryString = (fieldKeys: any) => {
  if (fieldKeys['order.name'] || fieldKeys['order.value']) {
    fieldKeys.order = {
      name: fieldKeys['order.name'],
      value: Number(fieldKeys['order.value']),
    };

    delete fieldKeys['order.name'];
    delete fieldKeys['order.value'];
  }

  // Object.keys(fieldKeys).forEach(key => {
  //   if (Number(fieldKeys[key]) === 0 || Number(fieldKeys[key])) {
  //     fieldKeys[key] = Number(fieldKeys[key]);
  //   }
  // });

  Object.keys(fieldKeys).forEach(key => {
    if (fieldKeys[key] === '') {
      delete fieldKeys[key];
    }
  });

  if (fieldKeys.page || fieldKeys.take) {
    fieldKeys.page = Number(fieldKeys.page);
    fieldKeys.take = Number(fieldKeys.take);
  }

  if (fieldKeys['where.status']) {
    fieldKeys['where.status'] = Number(fieldKeys['where.status']);
  }

  if (fieldKeys['where.product.status']) {
    fieldKeys['where.product.status'] = Number(
      fieldKeys['where.product.status'],
    );
  }

  if (fieldKeys['where.objectType']) {
    fieldKeys['where.objectType'] = Number(fieldKeys['where.objectType']);
  }

  if (fieldKeys['where.totalStep']) {
    fieldKeys['where.totalStep'] = Number(fieldKeys['where.totalStep']);
  }
};

export const handleUploadSingleImage = async (fileList: any[], action: (e: { file: any }) => void) => {
  if (!fileList || (Array.isArray(fileList) && fileList.length === 0)) {
    return undefined;
  }
  let result: any = undefined;

  if (fileList.length) {
    result = [...fileList];
    try {
      await Promise.all(
        fileList.map(async (item, index) => {
          if (item.originFileObj) {
            const actionResult: any = await action({ file: item.originFileObj });
            const { data } = unwrapResult(actionResult);
            const dataFormat = {
              uid: Date.now(),
              url: data[0].url[0],
              urlThumbnail: data[0].urlThumbnail[0],
              isNew: true,
              type: 0,
            };
            result.splice(index, 1, dataFormat);
          }
        }),
      );
    } catch (error: any) {
      Notification('error', 'Tải ảnh thất bại');
    }
  } else {
    try {
      const actionResult: any = await action({ file: fileList });
      const { data } = unwrapResult(actionResult);
      result = data;
    } catch (error: any) {
      Notification('error', 'Tải ảnh thất bại');
    }
  }

  return result;
};

export const handleUploadMultiImage = async (fileList: any[], action: (e: { file: any }) => void) => {
  if (!fileList || (Array.isArray(fileList) && fileList.length === 0)) {
    return undefined;
  }
  let result: any = undefined;

  if (fileList.length) {
    const fileData = fileList.map(item => item.originFileObj);
    try {
      const actionResult: any = await action({ file: fileData });
      const { data } = unwrapResult(actionResult);
      const formatData = data[0]?.url?.map((item, index) => ({
        isNew: true,
        type: 0,
        uid: Date.now(),
        url: item,
        urlThumbnail: data[0]?.urlThumbnail[index]
      }))
      result = formatData
    } catch (error: any) {
      Notification('error', 'Tải ảnh thất bại');
    }
  } else {
    try {
      const actionResult: any = await action({ file: fileList });
      const { data } = unwrapResult(actionResult);
      result = data;
    } catch (error: any) {
      Notification('error', 'Tải ảnh thất bại');
    }
  }
  return result;
};

export const handleUploadVideo = async (fileList: any[], action: any) => {
  if (!fileList || fileList === undefined) {
    return undefined;
  }
  let result: any = undefined;

  try {
    const actionResult: any = await action();
    const { data } = unwrapResult(actionResult);
    result = {
      ...data,
      isVideo: true,
    };
  } catch (error: any) {
    Notification('error', 'Tải video thất bại');
  }
  return result;
};

const unitPrefixs = {
  0: '',
  1: 'bịch',
  2: 'hộp',
  3: 'vỉ',
  4: 'phần',
  5: 'cái',
  6: 'thùng',
  7: 'con',
  8: 'hũ',
  9: 'chai',
  100: 'khác',
}

const unitPostfixs = {
  0: 'gr',
  1: 'kg',
  2: 'yến',
  3: 'tạ',
  4: 'tấn',
  5: 'cm',
  6: 'm',
  7: 'quả',
  8: 'bịch',
  9: 'ml',
  10: 'l',
  100: 'khác',
}

type unittype = {
  unitPrefix: number;
  unitWeight: number;
  unitPostfix: number;
}

export const genUnit = (unit: unittype) => {
  return unitPrefixs[unit.unitPrefix] + ' ' + unit.unitWeight + ' ' + unitPostfixs[unit.unitPostfix];
}