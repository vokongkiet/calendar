import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { App } from 'containers/App';
import { LoginPage } from 'containers/App/Auth/LoginPage/Loadable';

export const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/login" component={LoginPage} />
        <Route path="/" component={App} />
      </Switch>
    </BrowserRouter>
  );
};
