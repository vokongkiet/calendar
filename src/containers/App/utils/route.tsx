import { DashboardPage } from 'containers/WorkSpace/Dashboard/Loadable';
import { TransactionPage } from 'containers/WorkSpace/Transaction/Loadable';

export interface RouteProps {
  path?: string;
  component: any;
  render?: (props: any) => void;
  exact?: boolean;
}

export const routes = () => {
  return [
    {
      exact: true,
      path: `/`,
      component: DashboardPage,
    },
    {
      exact: true,
      path: `/statistical`,
      component: DashboardPage,
    },
    {
      exact: true,
      path: `/transaction`,
      component: TransactionPage,
    },
    {
      path: `/partner/customer`,
      component: DashboardPage,
    },
    {
      path: `/partner/supplier`,
      component: DashboardPage,
    },
  ];
};
