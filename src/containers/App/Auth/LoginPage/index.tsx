import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Form, Carousel, Input } from 'antd';
import { GlobalStyle } from 'styled';
import {
  PhoneOutlined,
  LockOutlined,
  CloseCircleOutlined,
  Password,
  Icon,
} from 'farmhub-ui-core';
import { RootState } from 'app';
import { ButtonType, HtmlType, Space, Text } from 'farmhub-ui-core';
import { CButton, TitleText } from 'components';
import { isVietnamesePhoneNumber, formatPhoneNumber } from 'utils/function';
import { login } from 'containers/App/Auth/authSlice';

import logoFullTextBeta from 'assets/images/logo_full_text_beta.png';
import introFarmer from 'assets/images/intro_farmer.png';
import introCertification from 'assets/images/intro_ceftification.png';
import introEcommerce from 'assets/images/intro_ecommerce.png';

export const LoginPage = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const auth = useSelector((state: RootState) => state.auth);

  const [isLoginError, setIsLoginError] = useState(false);
  const [isPhoneNumber, setIsPhoneNumber] = useState(true);

  const handleVerifyPhone = e => {
    setIsPhoneNumber(isVietnamesePhoneNumber(e.target.value));
  };

  const handleSummit = async values => {
    history.push('/');
    if (!isPhoneNumber) {
      return;
    }
    const accountBody: any = {
      phoneNumber: formatPhoneNumber(values.phoneNumber),
      password: values.password,
    };

    try {
      const actionResult: any = await dispatch(login(accountBody));
      unwrapResult(actionResult);

      setIsLoginError(false);
    } catch (error) {
      setIsLoginError(true);
    }
  };

  useEffect(() => {
    if (auth.isAuthenticated) {
      history.push('/');
    }
  }, [auth.isAuthenticated, history]);

  return (
    <WrapperLogin>
      <WrapperLeft>
        <WrapLogin>
          <LogoBar>
            <Logo src={logoFullTextBeta} />
            {/*<Space width={10} />*/}
            {/*<WrapperText>*/}
            {/*  <NameText style={{ fontSize: 26 }}>FarmCheck</NameText>*/}
            {/*</WrapperText>*/}
          </LogoBar>
          {isLoginError && (
            <WrapperError>
              <CloseCircleOutlined
                style={{
                  fontSize: '16px',
                  color: '#FF6347',
                  marginTop: 2,
                  marginRight: 12,
                }}
              />
              <span>Đăng nhập KHÔNG thành công bạn vui lòng thử lại</span>
            </WrapperError>
          )}
          <CForm
            name="normal_login"
            className="login-form"
            onFinish={handleSummit}
          >
            <CFormItem
              help={
                isPhoneNumber ? null : (
                  <span style={{ color: 'red' }}>
                    Số điện thoại không hợp lệ !
                  </span>
                )
              }
              name="phoneNumber"
              rules={[
                {
                  required: true,
                  message: 'Bạn chưa nhập số điện thoại!',
                },
              ]}
            >
              <Input
                onChange={handleVerifyPhone}
                prefix={
                  <Icon>
                    <PhoneOutlined />
                  </Icon>
                }
                placeholder="Số điện thoại"
              />
            </CFormItem>
            <CFormItem
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Bạn chưa nhập mật khẩu!',
                },
              ]}
            >
              <Password
                icon={{ children: <LockOutlined /> }}
                placeholder="Mật khẩu"
              />
            </CFormItem>
            <Form.Item>
              <CDirect>
                <span> Quên mật khẩu? </span>
              </CDirect>
              <CButton
                styles={{ 'margin-bottom': '10px' }}
                weight="bold"
                width="100%"
                height="50px"
                type={ButtonType.Primary}
                htmlType={HtmlType.Submit}
              >
                Đăng nhập
              </CButton>
            </Form.Item>
          </CForm>
        </WrapLogin>
      </WrapperLeft>
      <WrapperRight>
        <CarouselWrapper>
          <CCarousel>
            <div>
              <CarouselContent>
                <CarouselImg src={introEcommerce} />
                <Space height={50} />
                <TitleText>Sàn nông sản uy tín</TitleText>
                <Text
                  style={{
                    width: '415px',
                    textAlign: 'center',
                    marginTop: '24px',
                    marginBottom: '24px',
                  }}
                >
                  Trở thành một sàn thương mại nông sản uy tín cho người Việt,
                  nơi kết nối giữa những người nông dân chân chính và người bán
                  hàng có tâm, với mong muốn đóng góp cho ngành nông nghiệp sạch
                  - hữu cơ nước nhà và hỗ trợ nông dân địa phương.
                </Text>
              </CarouselContent>
            </div>
            <div>
              <CarouselContent>
                <CarouselImg src={introFarmer} />
                <Space height={50} />
                <TitleText>Mắt xích quan trọng</TitleText>
                <Text
                  style={{
                    width: '415px',
                    textAlign: 'center',
                    marginTop: '24px',
                    marginBottom: '24px',
                  }}
                >
                  Là một mắt xích quan trọng trong hệ sinh thái nông nghiệp Việt
                  Nam, có giải pháp liên kết tối ưu giữa những người đang tham
                  gia trong ngành nông nghiệp, kiên trì và dành tất cả các nguồn
                  lực để tạo ra một nền tảng có các tính năng hiệu quả.
                </Text>
              </CarouselContent>
            </div>
            <div>
              <CarouselContent>
                <CarouselImg src={introCertification} />
                <Space height={50} />
                <TitleText>Kiểm định chất lượng</TitleText>
                <Text
                  style={{
                    width: '415px',
                    textAlign: 'center',
                    marginTop: '24px',
                    marginBottom: '24px',
                  }}
                >
                  Nông sản lên sàn sẽ được kiểm định chất lượng bởi các đơn vị
                  uy tín, minh bạch chất lượng và truy xuất nguồn gốc rõ ràng,
                  với quy trình kiểm soát chéo giữa các bên: nông trại & đại lý
                  & đơn vị kiểm định định kỳ để đảm bảo chất lượng và tính minh
                  bạch, tránh ảnh hưởng đến sức khỏe người tiêu dùng và nâng cao
                  giá trị nông sản Việt trên trường Quốc Tế.
                </Text>
              </CarouselContent>
            </div>
          </CCarousel>
        </CarouselWrapper>
      </WrapperRight>
      <GlobalStyle />
    </WrapperLogin>
  );
};

const WrapperLogin = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const WrapperLeft = styled.div`
  width: 40%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.1);
`;

const WrapperRight = styled.div`
  width: 60%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const WrapLogin = styled.div`
  width: 100%;
  height: auto;
  background: #fff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const CForm = styled(Form)`
  width: 360px;
`;

const CFormItem = styled(Form.Item)`
  min-height: 50px;

  .ant-form-item-control-input {
    height: 50px;

    .ant-form-item-control-input-content {
      height: 50px;

      .ant-input-affix-wrapper {
        height: 50px;
        border-radius: 6px;

        .ant-input {
          &::placeholder {
            color: #595959 !important;
          }
        }
      }
    }
  }
`;

const CDirect = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 24px;
  a {
    margin-left: 6px;
  }
`;

const LogoBar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: auto;
  width: auto;
  margin-bottom: 62px;
`;

const Logo = styled.img`
  width: 300px;
  height: auto;
`;

const CCarousel = styled(Carousel)`
  .slick-dots {
    margin-left: 0px !important;
  }
`;

const CarouselWrapper = styled.div`
  width: 500px;
  height: 500px;
`;

const CarouselImg = styled.img`
  width: auto;
  height: 240px;
  border-radius: 6px;
`;

const CarouselContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 500px;
`;

const WrapperError = styled.div`
  display: flex;
  width: 360px;
  height: auto;
  background-color: #fff9fa;
  border: 1px solid rgba(255, 66, 79, 0.2);
  border-radius: 6px;
  padding: 12px 15px;
  margin-bottom: 20px;
`;
