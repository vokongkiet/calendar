/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { Layout } from 'antd';
import { Switch, Route, useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalStyle } from 'styled';
import { RootState } from 'app';
import { Space } from 'farmhub-ui-core';
import { HeaderMainNavbar } from 'components';
import { BackMenu, MenuItem, MenuTitle } from './components';
import { logout } from 'containers/App/Auth/authSlice';
import * as S from './Styles';
import { dataMenu } from './utils/function';
import logoFullTextBeta from 'assets/images/logo_full_text_beta.png';
import logoBeta from 'assets/images/logo_beta.png';
import { routes } from './utils/route';
import { updateMenuToggle } from './Account/entitySlice';

export function App() {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const isToggle = useSelector((state: RootState) => state.entity.isToggle);

  const [menuActive, setMenuActive] = useState('');
  const [currentDataMenu, setCurrentDataMenu] = useState<any>(dataMenu());

  const handleLogoutUser = async () => {
    history.push('/login');
    await dispatch(logout());
  };

  const handleSelectMenu = (item: any) => {
    history.push(item.link);
    if (item.subMenu.length === 0) {
      setMenuActive(item.link);
      return;
    }

    setMenuActive(item.link);
    setCurrentDataMenu(item.subMenu);
  };

  const formatPath = (path: string) => {
    return path;
  };

  const handleSetMenuActive = (path: string) => {
    const newPath = formatPath(path);
    if (newPath === `/`) {
      setMenuActive(path);
    } else {
      setMenuActive(newPath);
    }
  };

  const handleBackMenu = (layer: number) => {
    if (layer === 3) {
      history.goBack();
      return;
    }
    history.push(`/`);
  };

  const renderMenu = () => {
    const result: any = [];
    let menuFilter: any;
    if (currentDataMenu[0].layer !== 1) {
      result.push(
        <BackMenu
          isToggle={isToggle}
          onClick={() => handleBackMenu(currentDataMenu[0].layer)}
        />,
      );
    }
    for (let i = 0; i < currentDataMenu.length; i += 1) {
      if (isToggle) {
        result.push(
          <>
            {i !== 0 && <Space height={12} />}
            <MenuTitle title={currentDataMenu[i].title} />
          </>,
        );
      }
      for (let j = 0; j < currentDataMenu[i].menu?.length; j += 1) {
        const menuItem = currentDataMenu[i].menu[j];
        if (!menuItem.hidding) {
          result.push(
            <MenuItem
              isToggle={isToggle}
              menuItem={menuItem}
              menuActive={menuActive}
              onClick={() => handleSelectMenu(menuItem)}
            />,
          );
        }
      }
    }
    if (menuFilter) {
      result.push(menuFilter);
    }

    return result;
  };

  const handleWindowResize = () => {
    if (window.innerWidth >= 1450) {
      dispatch(updateMenuToggle(true));
    } else {
      dispatch(updateMenuToggle(false));
    }
  };

  useLayoutEffect(() => {
    handleWindowResize();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useLayoutEffect(() => {
    window.addEventListener('resize', handleWindowResize);
    return () => window.removeEventListener('resize', handleWindowResize);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const initMenuData = (initDataMenu: any) => {
      const currentPath = location.pathname;
      handleSetMenuActive(currentPath);
      for (let i = 0; i < initDataMenu.length; i += 1) {
        for (let j = 0; j < initDataMenu[i].menu.length; j += 1) {
          const menuItem = initDataMenu[i].menu[j];
          if (menuItem.link.includes(formatPath(currentPath))) {
            if (menuItem.subMenu.length === 0) {
              setCurrentDataMenu(initDataMenu);
              return;
            } else {
              setCurrentDataMenu(menuItem.subMenu);
              return;
            }
          } else {
            initMenuData(menuItem.subMenu);
          }
        }
      }
    };

    const initDataMenu = dataMenu();
    initMenuData(initDataMenu);
  }, [location]); //eslint-disable-line

  useEffect(() => {
    if (location.pathname === '/') {
      history.push(`/`);
    }
  }, [history, location.pathname]);

  const ref: any = useRef(null);

  return (
    <S.Wrapper>
      {/* <Tool /> */}
      <S.WrapperMenu ref={ref} isToggle={isToggle}>
        <S.HeadWrapper>
          <S.WrapperLogo
            onClick={() => {
              history.push(`/`);
            }}
          >
            {isToggle === false ? (
              <S.Logo src={logoBeta} />
            ) : (
              <S.LogoFullText src={logoFullTextBeta} />
            )}
          </S.WrapperLogo>
        </S.HeadWrapper>
        <Space height={12} />
        <S.ContentMenu>
          <S.CMenu>{renderMenu()}</S.CMenu>
        </S.ContentMenu>
      </S.WrapperMenu>
      <Layout style={{ background: '#F8F8F8' }}>
        <HeaderMainNavbar onLogoutUser={handleLogoutUser} />
        <S.WrapperContent>
          <S.Content width="100%">
            <Switch>
              {routes().map((item: any, index: number) => (
                <Route
                  key={index}
                  exact={item.exact}
                  path={item.path}
                  component={item.component}
                  render={item.render}
                />
              ))}
            </Switch>
          </S.Content>
        </S.WrapperContent>
      </Layout>
      <GlobalStyle />
    </S.Wrapper>
  );
}
