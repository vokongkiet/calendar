import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Dropdown, Menu } from 'antd';
import { IconWrapper } from 'components';

interface IDataItem {
  name: string;
  link?: any; //TODO: research type of link
  action?: () => void;
  key: string;
  disabled?: boolean;
  icon: any;
}

interface Props {
  data: Array<IDataItem>;
  component: any;
}

const ExtendedAction = ({ data, component }: Props): JSX.Element => {
  const menu = () => (
    <Menu>
      {data.map((item: IDataItem) => {
        if (item.link) {
          return (
            <Link key={item.key} to={item.link}>
              <CMenuItem disabled={item.disabled}>{item.name}</CMenuItem>
            </Link>
          );
        } else {
          return (
            <CMenuItem onClick={item.action} key={item.key}>
              <IconWrapper
                hoverBackground="rgba(57, 181, 74, 0.2)"
                size={36}
                background="#F1F1F1"
                icon={item.icon}
              />
              {item.name}
            </CMenuItem>
          );
        }
      })}
    </Menu>
  );

  return (
    <Wrapper onClick={(e: any) => e.stopPropagation()}>
      <Dropdown trigger={['click']} overlay={menu()} placement="bottomLeft">
        {component}
      </Dropdown>
    </Wrapper>
  );
};

export default ExtendedAction;

const CMenuItem = styled(Menu.Item)`
  padding: 6px 18px 6px 6px !important;
  font-weight: ${props => props.theme.font.weight.regular} !important;
  color: ${props => props.theme.colors.text.secondary};

  .ant-dropdown-menu-title-content {
    display: flex;
    align-items: center;
  }
`;

const Wrapper = styled.div``;
