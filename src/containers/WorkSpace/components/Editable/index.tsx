import React, { useContext, useEffect, useRef, useState } from 'react';
import { Form } from 'antd';
import { InputNumber } from 'farmhub-ui-core';

const EditableContext = React.createContext(null);

export const EditableRow = ({ index, ...props }) => {
  const [form]: any = Form.useForm<any>();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

export const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const inputRef: any = useRef(null);
  const form: any = useContext(EditableContext);
  const [editing, setEditing] = useState(false);

  useEffect(() => {
    if (editing) {
      inputRef.current?.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      const oldCustomerQuantity = record.customerQuantity;
      handleSave({ ...record, ...values, oldCustomerQuantity });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
      >
        <InputNumber
          color="green"
          height="46px"
          width="100px"
          ref={inputRef}
          onPressEnter={save}
          onBlur={save}
          defaultValue={0}
          min={0}
          fontWeight="bold"
        />
      </Form.Item>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};
