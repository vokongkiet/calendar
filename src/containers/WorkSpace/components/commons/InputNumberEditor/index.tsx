import React, { useRef, useState } from 'react';
import { InputNumber } from 'farmhub-ui-core';
import styled from 'styled-components';

interface InputNumberEditorProps {
  id: string;
  initValue: number;
  color?: string;
  onUpdate: (id: string, value: number, oldValue: number) => any;
  step?: number;
  disabled?: boolean;
  styles?: any;
  placeholder?: string;
  max?: number;
}

const InputNumberEditor = ({
  id,
  initValue,
  color,
  onUpdate,
  step,
  disabled,
  styles,
  placeholder,
  max,
}: InputNumberEditorProps) => {
  const [value, setValue] = useState(initValue);
  const [isDisabled, setIsDisabled] = useState(disabled || false);
  const oldValueRef = useRef(initValue);

  const handleChange = (value: any) => {
    setValue(value);
  };

  const handleUpdate = async () => {
    if (oldValueRef.current === value) {
      return;
    }
    setIsDisabled(true);
    const result = await onUpdate(id, value, oldValueRef.current);
    if (result?.success || !result) {
      oldValueRef.current = value;
    } else {
      setValue(oldValueRef.current);
    }
    setIsDisabled(false);
  };

  return (
    <Wrapper onClick={(e: any) => e.stopPropagation()}>
      <InputNumber
        placeholder={placeholder ? placeholder : undefined}
        disabled={isDisabled}
        fontWeight="bold"
        min={0}
        height="46px"
        width="100px"
        color={color || 'primary'}
        value={value?.toString()}
        onChange={handleChange}
        onBlur={handleUpdate}
        onPressEnter={handleUpdate}
        step={step || 1}
        styles={styles || {}}
        max={max || undefined}
      />
    </Wrapper>
  );
};

export default InputNumberEditor;

const Wrapper = styled.div``;
