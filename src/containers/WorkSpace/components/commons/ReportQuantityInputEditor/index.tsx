import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { InputNumber } from 'farmhub-ui-core';
import { Form, Input } from 'antd';
import { CSelect, FormItem } from 'components';

const count = [
  { key: '0', value: '0', name: 'SL tương đối' },
  { key: '1', value: '1', name: 'SL tuyệt đối' },
];

const quantity = [
  { key: '0', value: '0', name: 'Hết hàng' },
  { key: '1', value: '1', name: 'Có ít' },
  { key: '2', value: '2', name: 'Có vừa' },
  { key: '3', value: '3', name: 'Có nhiều' },
];

interface reportQuantityBoxType {
  id: string;
  reportType: number;
  relativeQuantity: number;
  absoluteQuantity: number;
}

interface ReportQuantityInputEditorProps {
  reportQuantityBox: reportQuantityBoxType;
  onUpdate: (id: string, body: any) => void;
}

const ReportQuantityInputEditor = ({
  reportQuantityBox,
  onUpdate,
}: ReportQuantityInputEditorProps) => {
  const [form] = Form.useForm();
  const [type, setType] = useState('0');

  const handleUpdateReportQuantity = async () => {
    form.validateFields().then(async (values: any) => {
      Object.keys(values).forEach(
        (key: string) => (values[key] = Number(values[key])),
      );

      onUpdate(reportQuantityBox.id, values);
    });
  };

  useEffect(() => {
    const initData = () => {
      const newType = `${reportQuantityBox?.reportType}`;
      setType(newType);

      form.setFieldsValue({
        reportType: `${reportQuantityBox?.reportType}`,
        relativeQuantity: `${reportQuantityBox?.relativeQuantity}`,
        absoluteQuantity: `${reportQuantityBox?.absoluteQuantity}`,
      });
    };

    initData();
  }, [reportQuantityBox]);

  return (
    <Wrapper onClick={(e: any) => e.stopPropagation()}>
      <Form style={{ marginBottom: 0 }} form={form}>
        <Input.Group onBlur={handleUpdateReportQuantity} compact>
          <FormItem
            margininer="0px"
            style={{ marginBottom: 0 }}
            name="reportType"
          >
            <CSelect
              weight="bold"
              height="46px"
              width="130px"
              defaultValue="0"
              list={count}
              onChange={setType}
            />
          </FormItem>
          {type === '0' ? (
            <FormItem
              margininer="0px"
              style={{ marginBottom: 0 }}
              name="relativeQuantity"
            >
              <CSelect
                weight="bold"
                height="46px"
                width="130px"
                defaultValue="0"
                color="red"
                list={quantity}
              />
            </FormItem>
          ) : (
            <FormItem
              margininer="0px"
              style={{ marginBottom: 0 }}
              name="absoluteQuantity"
            >
              <InputNumber
                width="130px"
                defaultValue={0}
                height="46px"
                fontWeight="bold"
              />
            </FormItem>
          )}
        </Input.Group>
      </Form>
    </Wrapper>
  );
};

export default ReportQuantityInputEditor;

const Wrapper = styled.div``;
