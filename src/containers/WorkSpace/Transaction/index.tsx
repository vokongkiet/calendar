import React, { useState } from 'react';
import ReceivedMoneyTab from './ReceivedMoneyTab';
import { IconWrapper, PageTitleBar, Wrapper } from 'components';
import { Tabs, TabPane, Icon } from 'farmhub-ui-core';
import { FilterFilled } from 'farmhub-ui-core';

export const TransactionPage = (): JSX.Element => {
  const [isDisplayFilter, setIsDisplayFilter] = useState(true);

  return (
    <Wrapper>
      <PageTitleBar bottom="12px" title="Giao dịch" url="/transaction" />
      <Tabs
        tabBarExtraContent={
          <IconWrapper
            displayBackground={true}
            background={isDisplayFilter ? '' : '#f1f1f1'}
            onClick={() => setIsDisplayFilter(!isDisplayFilter)}
            icon={
              <Icon>
                {' '}
                <FilterFilled
                  style={{
                    color: isDisplayFilter ? '#39B54A' : '',
                    fontSize: 16,
                  }}
                />
              </Icon>
            }
            size={42}
          />
        }
        marginBottom="0px"
        defaultActiveKey="1"
      >
        <TabPane tab="Số tiền nhận" key="1">
          <ReceivedMoneyTab isDisplayFilter={isDisplayFilter} />
        </TabPane>
        <TabPane tab="Số tiền nợ" key="2">
          <ReceivedMoneyTab isDisplayFilter={isDisplayFilter} />
        </TabPane>
      </Tabs>
    </Wrapper>
  );
};
