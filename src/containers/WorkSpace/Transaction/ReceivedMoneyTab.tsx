import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Form } from 'antd';

import { Text } from 'farmhub-ui-core';
import {
  dataEmpty,
  Empty,
  FilterTable,
  NameText,
  PriceText,
  Table,
} from 'components';
import { formatMoney, renderTableSkeleton } from 'utils/function';

const orders = [
  { key: '0', value: 'createdAt', name: 'Sắp theo ngày tạo' },
  { key: '1', value: 'customer.displayName', name: 'Sắp theo Alphabet' },
];

const orderItemQuery = {
  page: 1,
  take: 10,
  search: '',
  relations: ['order', 'product', 'order.customer'],
  select: [
    'id',
    'customerReceivedQuantity',
    'order.id',
    'customer.id',
    'customer.displayName',
    'product.id',
    'product.price',
    'product.unitPrefix',
    'product.unitWeight',
    'product.unitPostfix',
    'price',
    'createdAt',
  ],
  order: { name: 'createdAt', value: -1 },
};

const columns: Array<any> = [
  {
    title: 'Tên người mua',
    dataIndex: 'order',
    key: 'name',
    render: (order: any) => <NameText>Nguyễn Văn A</NameText>,
  },
  {
    title: 'Số lượng',
    dataIndex: 'quantityBox',
    key: 'count',
    render: (quantityBox: any) => (
      <>
        <NameText>12kg</NameText>
      </>
    ),
  },
  {
    title: 'Giá bán',
    dataIndex: 'product',
    key: 'price',
    render: (product: any) => <PriceText>{formatMoney(200000)}đ</PriceText>,
  },
  {
    title: 'Tổng tiền',
    dataIndex: 'price',
    key: 'sum',
    render: (price: number) => <PriceText>{formatMoney(200000)}đ</PriceText>,
  },
  {
    title: 'Ngày mua',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (createdAt: string) => <Text>12/12/2021</Text>,
  },
  {
    title: 'Tiền hoa hồng',
    dataIndex: 'bonus',
    key: 'bonus',
    render: () => <PriceText>0đ</PriceText>,
  },
];

interface ReceivedMoneyTabProps {
  isDisplayFilter: boolean;
}

const ReceivedMoneyTab = ({
  isDisplayFilter,
}: ReceivedMoneyTabProps): JSX.Element => {
  const [form] = Form.useForm();
  const [query, setQuery] = useState({
    ...orderItemQuery,
    'where.order.supplierID': '123',
  });

  const dataFilter = [
    {
      name: 'order',
      list: orders,
    },
  ];

  const handleFilterFormChange = e => {
    if (e.isObject) {
      return;
    }

    const key = Object.keys(e)[0];
    const value = Object.values(e)[0];

    if (value === 'all') {
      delete query[key];
      setQuery({ ...query });
    } else {
      setQuery({ ...query, ...e });
    }
  };

  const handlePaginationChange = e => {
    setQuery({
      ...query,
      page: e.current,
      take: e.pageSize,
    });
  };

  useEffect(() => {
    const initData = () => {
      form.setFieldsValue({ order: { name: 'createdAt', value: -1 } });
    };
    initData();
  }, [form]);

  let locale = {
    emptyText: (
      <Empty
        minHeight={320}
        img={dataEmpty.REVENUE}
        description="Hiện tại không có giao dịch"
      />
    ),
  };

  return (
    <Wrapper>
      <FilterTable
        name="Người mua"
        searchName="where.order.customer.displayName.fs"
        form={form}
        isDisplayFilter={isDisplayFilter}
        data={dataFilter}
        handleFilterFormChange={handleFilterFormChange}
      />
      <Table
        onChange={handlePaginationChange}
        locale={locale}
        columns={false ? renderTableSkeleton(skeletonData) : columns}
        dataSource={[{}, {}, {}, {}, {}]}
      />
    </Wrapper>
  );
};

export default ReceivedMoneyTab;

const Wrapper = styled.div`
  width: 100%;
  height: auto;
  border-radius: 6px;
`;

const skeletonData = [
  {
    title: 'Tên người mua',
    type: 'input',
    width: 130,
  },
  {
    title: 'Số lượng',
    type: 'input',
    width: 70,
  },
  {
    title: 'Giá bán',
    type: 'input',
    width: 70,
  },
  {
    title: 'Tổng tiền',
    type: 'input',
    width: 100,
  },
  {
    title: 'Ngày mua',
    type: 'input',
    width: 100,
  },
  {
    title: '	Tiền hoa hồng',
    type: 'input',
    width: 70,
  },
];
