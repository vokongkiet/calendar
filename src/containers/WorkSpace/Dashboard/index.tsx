import { Col, Row } from 'antd';
import { LineChart, Statistics } from 'components';
import {
  CartIcon,
  MegaphoneIcon,
  RequestIcon,
  WaitingIcon,
} from 'components/Icon';
import { OverViewCard } from 'containers/WorkSpace/components';
import styled from 'styled-components';

export const DashboardPage = (): JSX.Element => {
  const dataCard: Array<any> = [
    {
      name: 'Yêu cầu liên kết',
      count: 0,
      background: 'rgba(57, 181, 74, 0.1)',
      color: '#39B54A',
      icon: <RequestIcon width={21} height={21} />,
    },
    {
      name: 'Đơn hàng đang chờ',
      count: 0,
      background: 'rgba(85, 192, 235, 0.1)',
      color: '#55C0EB',
      icon: <WaitingIcon width={25} height={25} />,
    },
    {
      name: 'Báo hàng hôm nay',
      count: 0,
      background: 'rgba(250, 173, 20, 0.1)',
      color: '#FAAD14',
      icon: <CartIcon width={25} height={25} />,
    },
    {
      name: 'Chiến dịch đang diễn ra',
      count: 0,
      background: ' rgba(255, 87, 115, 0.1)',
      color: '#FF5773',
      icon: <MegaphoneIcon width={22} height={22} />,
    },
  ];
  return (
    <Wrapper>
      <Row gutter={[18, 18]}>
        <Statistics />
        {dataCard.map((item: any, index: number) => (
          <Col key={index} span={6}>
            <OverViewCard
              info={{ name: item.name, count: item.count }}
              style={{
                color: item.color,
                background: item.background,
                icon: item.icon,
              }}
            />
          </Col>
        ))}
        <Col span={24}>
          <WrapperChart>
            <LineChart />
          </WrapperChart>
        </Col>
      </Row>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  height: auto;
`;

const WrapperChart = styled.div`
  width: 100%;
`;
