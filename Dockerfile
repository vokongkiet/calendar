FROM node:14-buster-slim AS builder

RUN apt update
RUN apt -y install vim curl git xsel
RUN yarn set version 1.22.10

WORKDIR /app

COPY . .

ENV NODE_OPTIONS=--max_old_space_size=16384

RUN yarn install
RUN yarn build

CMD ["yarn", "serve", "-s", "build"]

EXPOSE 5000
